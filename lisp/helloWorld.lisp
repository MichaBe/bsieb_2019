;Basic ausgabe
(write "Hallo Welt")
(write-line "Hallo Welt 2")

; Zuweisung von Variablen
; setq: setzt variable auf wert
(write-line "")
(write-line "setq")
(setq myVar 10)
(write myVar)
(setq myVar 100)
(write myVar)

(write-line "")
(write-line "defvar")
;defvar: setzt nur einmal.
(defvar test 100)
(defvar test 50)
(write test)

(write-line "")
(write-line "Arithmetik")
(write (+ 1 2 3 4 5 6))
(write-line " ")
(write (- 10 5 5))
(write-line "")
(write (/ 16 4 2)); (16/4)/2 ...
(write (* 1 2 3 4))

(write-line "")
(write-line "let & prog")
;lokale variablen. Können nach dieser zeile nicht mehr zugegriffen werden
(let 
    ((v1 1) (v2 2) (v3 3) (v4 (+ 15 5)))
    (write 
        (+ v1 v2 v3 v4)
    )
)
(write-line "")

(prog
    ((x 100)(y 50))
    (write-line "Hi prog :)")
    (write (+ x y))
    (write-line "")
    (write-line "Das wars auch schon")
    (return T)
)

(write-line "")
(write-line "Konstanten")
(defconstant PI 3.1415926535)
(defun area-circle(radius)
    (* PI radius radius)
)
(write (area-circle 10))
(write t)