package applications;

public abstract class Kernelapplication {

    private String name;

    public String getName() {
        return name;
    }

    public Kernelapplication(String pName) {
        name = pName;
    }

    public abstract void init();

    public abstract boolean run();

    public abstract void render();

    public abstract Kernelapplication next();

    public abstract void handleKeyboardInput(char character,int keycode);
}
