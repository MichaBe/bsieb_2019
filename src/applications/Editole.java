package applications;

import applications.lispEditole.InteractiveEditor;
import applications.lispEditole.LispConsole;
import applications.lispEditole.LispEditor;
import keyboard.Key;
import lisp.Reader;
import lisp.data.Expression;
import screen.Screenwriter;

public class Editole extends Kernelapplication {
    boolean isRunning;
    boolean needsredraw;
    private boolean evaluateNow = false;

    private Reader reader;
    private Expression fromReader;
    private Expression fromEvaluator;
    private Screenwriter sw;

    private final static int usableOutputLines = 25-2;
    private final static int OffsetInputLine = 2;

    private boolean curModeIsConsole = true;//Start mit der konsole

    InteractiveEditor console;
    InteractiveEditor editor;
    InteractiveEditor curActive;

    //Konsole ----------------------------------------------------
    private final static String lastLineConsole = "[ENTER] evaluate  [F2] new session  [F3] goto Editor";

    //Editor -----------------------------------------------------
    private final static String lastLineEditor =  "[F1] evaluate     [F2] clear all    [F3] goto Console";


    public Editole() {
        super("Lisp-Editole");

        console = new LispConsole(23,lastLineConsole);
        editor = new LispEditor(23,lastLineEditor);
        curActive = console;
    }

    @Override
    public void init() {
        Screenwriter sw = new Screenwriter();
        sw.setColor(0x0f);
        sw.clear();
        isRunning = true;
        needsredraw = true;
        Screenwriter.setBlinkPosition(2,23);
    }

    @Override
    public boolean run() {
        if(isRunning && curActive.needsRerun())
            curActive.run();
        return isRunning;
    }

    @Override
    public void render() {
        if(curActive.needsRedraw() || needsredraw) {
            curActive.renderOutput();
            curActive.renderInputLine();
            curActive.renderHelpLine();
            needsredraw = false;
        }
    }

    @Override
    public Kernelapplication next() {
        return null;
    }

    @Override
    public void handleKeyboardInput(char character, int keycode) {
        //TODO nur esc und wechsel selbst behandeln. rest weiterleiten
        if(keycode == Key.ESC) {
            isRunning = false;
        }
        /*else if(keycode == Key.F3) {
            curModeIsConsole = !curModeIsConsole;
            if(curModeIsConsole)
                curActive = console;
            else
                curActive = editor;
        }*/
        else {
            curActive.handleInput(character, keycode);
        }
    }
}
