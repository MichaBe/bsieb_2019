package applications;

import compat.*;
import kernel.CONSTANTS;
import keyboard.Key;
import screen.Screenwriter;
import datastructures.BIOSBuffer;

public class Speicherlayout extends Kernelapplication {

    private boolean needsredraw;
    private boolean isRunning;
    public static final BIOSBuffer bb = (BIOSBuffer)MAGIC.cast2Struct(CONSTANTS.biosbufferAdress);

    public Speicherlayout() {
        super("Speicherlayoutabfrage");
    }

    @Override
    public void init() {
        needsredraw = true;
        isRunning = true;
    }

    @Override
    public boolean run() {
        return isRunning;
    }

    @Override
    public void render() {
        if(needsredraw) {
            isRunning = true;
            Screenwriter sw = new Screenwriter();
            sw.setColor(0xf,0x0);
            sw.clear();

            sw.println("System Memory Map:");
            sw.println("Unbelegte Speicherbloecke:");

            short esValue = (short)(CONSTANTS.biosbufferAdress /16);
            short ediValue = (short)(CONSTANTS.biosbufferAdress%esValue);
            BIOS.regs.EBX = 0;
            do {
                BIOS.regs.EAX = 0x0000E820;
                BIOS.regs.EDX = 0x534D4150;
                BIOS.regs.ECX = 24;
                BIOS.regs.ES = esValue;
                BIOS.regs.EDI = ediValue;
                BIOS.rint(0x15);

                if(BIOS.regs.EBX == 0 || BIOS.regs.ECX == 0 ||  (BIOS.regs.FLAGS&BIOS.F_CARRY) != 0)
                    break;

                if(bb.type == 0x01 && bb.base >= 0x100000) {
                    sw.print(' ');
                    sw.print("Base: ");
                    sw.printHex(bb.base);
                    sw.print(", Length: ");
                    sw.printHex(bb.length);
                    sw.println();
                }
            } while(BIOS.regs.EBX != 0);
            needsredraw = false;
        }
    }

    @Override
    public void handleKeyboardInput(char character, int keycode) {
        if(keycode== Key.ESC) {
            isRunning = false;
        }
    }

    @Override
    public Kernelapplication next() {
        return null;
    }
}
