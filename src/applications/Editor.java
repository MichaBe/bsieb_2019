package applications;

import interrupts.Interrupthandler;
import keyboard.Key;
import keyboard.KeyboardLowLevel;
import screen.Screen;
import screen.Screenwriter;

public class Editor extends Kernelapplication {

    private boolean finishedRunning;
    private boolean needsRedraw;

    private byte lines[][];
    private int curCursorX;
    private int curCursorY;

    public Editor() {
        super("Editor");
        needsRedraw = false;
        lines = new byte[24][];
        for(int i = 0; i < 24; ++i) {
            lines[i] = new byte[80];
            for (int j = 0; j < 80; ++j)
                lines[i][j] = 0;
        }
        curCursorX = curCursorY = 0;
        debugValue = 0;

    }

    @Override
    public void init() {
        finishedRunning = false;
        needsRedraw = true;
        Screen.switchToVGATextmode();
    }

    @Override
    public boolean run() {
        return !finishedRunning;
    }


    private static int debugValue;
    @Override
    public void render() {
        if(needsRedraw) {
            Screenwriter.directPrintCharArray(lines,0,0,0x0f);
            //Screenwriter.clear((byte)0x0f);
            //Screenwriter.directPrintInt(debugValue,10,0,0,0x0f);
            needsRedraw = false;
        }
    }

    @Override
    public void handleKeyboardInput(char character, int keycode) {
        debugValue++;
        if(keycode== Key.ENTER) {
            if(curCursorY != 23) {
                //alle zeilen unter aktueller zeile eine zeile nach untern verschieben
                for(int i = 23; i > curCursorY+1; --i) {
                    for(int j = 0; j < 80; ++j) {
                        lines[i][j] = lines[i-1][j];
                    }
                }
                //inhalt von aktueller position in nächste zeile verschieben
                for(int i = curCursorX, j = 0; i < 80; ++i, ++j) {
                    lines[curCursorY+1][j] = lines[curCursorY][i];
                    lines[curCursorY][i] = 0;
                }
                //cursor versetzen
                curCursorX = 0;
                ++curCursorY;
            }
        }
        else if(keycode==Key.ESC) {
            finishedRunning = true;
        }
        else if(keycode == Key.BACKSPACE) {
            if(curCursorX == 0) {
                lines[curCursorY][curCursorX] = 0;
                //Zeilen nach oben verschieben
                for(int i = curCursorY; i < 23; ++i) {
                    for(int j = 0; j < 80; ++j) {
                        lines[i][j] = lines[i+1][j];
                    }
                }
                //letzte zeile leeren
                for(int i = 0; i < 80; ++i)
                    lines[23][i] = 0;

                if(curCursorY > 0)
                    --curCursorY;

                for(int i = 79; i >= 0; --i) {
                    if(lines[curCursorY][i] != 0) {
                        curCursorX = i+1;
                        break;
                    }
                }
                //TODO zeichen hinter cursor mitnehmen
            }
            else {
                --curCursorX;
                lines[curCursorY][79] = 0;
                for(int i = curCursorX; i < 79; ++i)
                    lines[curCursorY][i] = lines[curCursorY][i+1];
            }
        }
        else if(character!= 0) {
            for(int i = 79; i > curCursorX; --i) {
                lines[curCursorY][i] = lines[curCursorY][i-1];
            }
            lines[curCursorY][curCursorX] = (byte)character;
            curCursorX = (curCursorX+1)%80;
            if(curCursorX == 0)
                curCursorY = (curCursorY+1)%24;
        }
        needsRedraw = true;
    }

    @Override
    public Kernelapplication next() {
        return null;
    }
}
