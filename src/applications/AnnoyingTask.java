package applications;

import screen.Screenwriter;

public class AnnoyingTask extends Kernelapplication {

    private int counter;

    public AnnoyingTask() {
        super("AnnoyingTask");
    }

    @Override
    public void init() {
        Screenwriter sw = new Screenwriter();
        sw.setColor(0x0, 0x9);
        sw.clear();
    }

    @Override
    public boolean run() {
        return true;
    }

    @Override
    public void render() {
        while(true) {
            Screenwriter.directPrintInt(counter, 3, 0, 15, 0x90);
            counter++;
        }
    }

    @Override
    public Kernelapplication next() {
        return null;
    }

    @Override
    public void handleKeyboardInput(char character, int keycode) {
    }
}
