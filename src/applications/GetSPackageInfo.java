package applications;

import compat.*;
import keyboard.Key;
import rte.SClassDesc;
import rte.SPackage;
import screen.Screenwriter;

public class GetSPackageInfo extends Kernelapplication {

    private boolean isRunning;
    private boolean needsRedraw;

    public GetSPackageInfo() {
        super("Get SPackage-Info");
        isRunning = false;
        needsRedraw = false;
    }

    @Override
    public void init() {
        isRunning = true;
        needsRedraw = true;
        //TODO
    }

    @Override
    public boolean run() {
        return isRunning;
    }

    @Override
    public void render() {
        if(needsRedraw) {
            Screenwriter sw = new Screenwriter();
            sw.setColor(0x0,0xf);
            sw.clear();

            SPackage curP = SPackage.root;
            int depth = 0;
            int unitCount;
            SClassDesc curUnit;
            for(int i = 0; i < 20 && curP != null; ++i) {
                for(int j = 0; j < depth; ++j)
                    sw.print(".");

                if(curP.name != null)
                    sw.print(curP.name);

                unitCount = 0;
                curUnit = curP.units;
                while (curUnit != null) {
                    unitCount++;
                    curUnit = curUnit.nextUnit;
                }
                sw.print(" (");
                sw.print(unitCount);
                sw.println(" Units)");

                if(curP.subPacks != null) {
                    curP = curP.subPacks;
                    depth++;
                }
                else if(curP.nextPack != null) {
                    curP = curP.nextPack;
                }
                else if(curP.outer != null && curP.outer != SPackage.root) {
                    curP = curP.outer.nextPack;
                    depth--;
                }
                else
                    break;
            }

            needsRedraw = false;
        }
    }

    @Override
    public Kernelapplication next() {
        return null;
    }

    @Override
    public void handleKeyboardInput(char character, int keycode) {
        if(keycode == Key.ESC) {
            isRunning = false;
        }
    }
}
