package applications;

import datastructures.LinkedObjectList;
import datastructures.LinkedObjectlistEntry;
import keyboard.Key;
import screen.Screen;
import screen.Screenwriter;

public class TaskManager extends Kernelapplication {

    private LinkedObjectList tasklist;
    private boolean needsredraw;
    private boolean finishedRunning;
    private Screenwriter sw;
    private int curInput;
    private Kernelapplication nextApp;

    public TaskManager() {
        super("Taskmanager");
        tasklist = new LinkedObjectList();
        sw = new Screenwriter();
        sw.setColor(0xf,0x0);
        nextApp = null;
    }

    public void remove(Object app) {
        LinkedObjectlistEntry first = tasklist.begin();
        int i = 0;
        while(first != null) {
            if(first.obj == app) {
                LinkedObjectlistEntry prev = first.previous;
                LinkedObjectlistEntry next = first.next;
                if(prev != null)
                    prev.next = next;
                if(next != null)
                    next.previous = prev;
                break;
            }
            first = first.next;
        }
    }

    @Override
    public void init() {
        needsredraw = true;
        finishedRunning = false;
        Screen.switchToVGATextmode();
        sw.setCursor(0,0);
    }

    @Override
    public boolean run() {
        if(finishedRunning) {
            if(0 <= curInput && curInput < tasklist.length()) {
                nextApp = (Kernelapplication)tasklist.at(curInput);
            }
            else if(curInput == 10) {
                nextApp = new Editor();
                tasklist.append(nextApp);
            }
            else if(curInput == 11) {
                nextApp = new GarbageCollectorWrapper();
                tasklist.append(nextApp);
            }
            else if(curInput == 12) {
                nextApp = new Speicherlayout();
                tasklist.append(nextApp);
            }
            else if(curInput == 13) {
                nextApp = new ProvokePageFault();
                tasklist.append(nextApp);
            }
            else if(curInput == 14) {
                nextApp = new GetSPackageInfo();
                tasklist.append(nextApp);
            }
            else if(curInput == 15) {
                nextApp = new AnnoyingTask();
                tasklist.append(nextApp);
            }
            else if(curInput == 16) {
                nextApp = new Editole();
                tasklist.append(nextApp);
            }
            else {//Falsche nummer eingegeben
                finishedRunning = false;
                needsredraw = true;
            }
            curInput = 0;
        }
        return !finishedRunning;
    }

    @Override
    public void render() {

        if(needsredraw) {
            sw.clear();
            sw.println("Taskmanager");
            LinkedObjectlistEntry e = tasklist.begin();
            int i = 0;
            while(e != null && i < 10) {
                sw.print(i);
                sw.println(((Kernelapplication)e.obj).getName());
                e = e.next;
                ++i;
            }

            if(e != null)
                sw.println("...");
            sw.print("Insgesamt ");
            sw.print(tasklist.length());
            sw.println(" laufende tasks");

            sw.println("Neuen Task anlegen:");
            sw.println("10 Editor");
            sw.println("11 GC-Wrapper");
            sw.println("12 Speicherlayoutabfrage");
            sw.println("13 Provoke Page Fault");
            sw.println("14 Get SPackage-Info");
            sw.println("15 Annoying Task");
            sw.println("16 Lisp-Editole");

            Screenwriter.directPrintString("                              ",0,24,0x0f);
            Screenwriter.directPrintInt(curInput,10,0,24,0x0f);

            needsredraw = false;
        }
    }

    @Override
    public Kernelapplication next() {
        needsredraw = true;
        Screenwriter.directPrintString("                              ",0,24,0x0f);
        return nextApp;
    }

    @Override
    public void handleKeyboardInput(char character, int keycode) {
        int enteredNumber = ((int)character)-48;
        if(0<= enteredNumber && enteredNumber <= 9) {
            curInput = curInput*10+enteredNumber;
        }
        else if(keycode== Key.BACKSPACE)
            curInput /=10;
        else if(keycode == Key.ENTER)
            finishedRunning = true;

        needsredraw = true;
    }
}
