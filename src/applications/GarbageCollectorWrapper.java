package applications;

import kernel.GarbageCollector;
import keyboard.Key;
import screen.Screenwriter;

public class GarbageCollectorWrapper extends Kernelapplication{
    private boolean isRunning;
    private boolean needsredraw;
    private boolean rerunGC;

    private Screenwriter sw;
    private GarbageCollector gc;

    public GarbageCollectorWrapper() {
        super("GC-Wrapper");
        sw = new Screenwriter();
        sw.setColor(0xA,0x0);
        gc = new GarbageCollector();
    }

    @Override
    public void init() {
        needsredraw = true;
        isRunning = true;
    }

    @Override
    public boolean run() {
        if(rerunGC) {
            gc.mark();
            gc.Collect();
            rerunGC = false;
            needsredraw = true;
        }
        return isRunning;
    }

    @Override
    public void render() {
        if(needsredraw) {
            sw.clear();
            sw.println("GC-Wrapper");
            sw.print("Total object count (incl. Heap- and Root-Objects): ");
            sw.println(gc._totalObjectCount);
            sw.print("Unused object count: ");
            sw.println(gc.unusedObjects.length());
            needsredraw = false;
        }
    }

    @Override
    public Kernelapplication next() {
        return null;
    }

    @Override
    public void handleKeyboardInput(char character, int keycode) {
        if(keycode == Key.ESC) {
            isRunning = false;
        }
        else if(keycode == Key.F9) {
            rerunGC = true;
        }
        else {
            sw.println(keycode);
        }
    }
}
