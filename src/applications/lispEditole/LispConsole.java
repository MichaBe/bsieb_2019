package applications.lispEditole;

import keyboard.Key;
import lisp.EvaluatorPrinter;
import lisp.Reader;
import lisp.data.Expression;
import lisp.data.list.List;
import screen.Screenwriter;

public class LispConsole extends InteractiveEditor {

    int curLine;

    private Reader lispReader;
    private Screenwriter writer;

    public LispConsole(int usableOutputLines, String help) {
        super(usableOutputLines, help);
        //TODO
        curLine = 0;
        lispReader = new Reader();
    }

    @Override
    public boolean handleInput(char character, int keycode) {
        if(!super.handleInput(character, keycode)) {
            //TODO
            if(keycode == Key.ENTER) {
                if(curLine >= inputLineNum-4) {
                    //TODO clear und wieder auf start
                    for(int i = 0; i < usableOutputLineCount; ++i) {
                        outputlines[i].clearInplace();
                        outputColors[i] = defaultColor;
                    }
                    curLine = 0;
                }

                outputColors[curLine] = Rcolor;
                outputlines[curLine].copyInplace("> ",0);
                outputlines[curLine].copyInplace(curInputLine,2);
                curLine++;

                Expression fromReader = lispReader.Read(curInputLine);
                if(fromReader != null) {
                    outputColors[curLine] = R2color;
                    outputlines[curLine].copyInplace("R>",0);
                    fromReader.print(outputlines[curLine],2);
                    curLine++;

                    Expression fromEval = EvaluatorPrinter.evaluate((List)fromReader);
                    if(fromEval != null) {
                        outputColors[curLine] = Pcolor;
                        outputlines[curLine].copyInplace("P>",0);
                        fromEval.print(outputlines[curLine],2);
                        curLine++;
                    }
                    else {
                        outputColors[curLine] = errorColor;
                        outputlines[curLine].copyInplace("EVAL-ERROR!",4);
                        curLine++;
                    }
                }
                else {
                    outputColors[curLine] = errorColor;
                    outputlines[curLine].copyInplace("Read-ERROR: ",4);
                    outputlines[curLine].copyInplace(lispReader.getLastRunstate(), 16);
                    curLine++;
                }
                curInputLine.clearInplace();
                curInputlineLength = 0;
                curCursorPos = 0;
                Screenwriter.setBlinkPosition(2,usableOutputLineCount);
                needsRerun = true;
                needsRedraw = true;
            }
            else if(keycode == Key.F2) {
                lispReader.reset();
            }
        }
        else {
            needsRedraw = true;
        }
        return true;
    }

    @Override
    public void run() {
        super.run();
    }
}
