package applications.lispEditole;

import keyboard.Key;

public class LispEditor extends InteractiveEditor {

    public LispEditor(int usableOutputLines, String help) {
        super(usableOutputLines, help);
    }

    @Override
    public boolean handleInput(char character, int keycode) {
        if(!super.handleInput(character,keycode)){
            //TODO
            if(keycode == Key.ARROW_DOWN || keycode == Key.ENTER) {
                //TODO zeile weiter unten als aktuelle nehmen
                //Text der aktuellen zeile speichern
                //Text counter erhöhen %
                //alte zeile in R-Farbe schreiben
                //neue zeile in Edit-Farbe schreiben
                //Text in editierzeile übernehmen
                //blink setzen

            }
            else if(keycode == Key.ARROW_UP) {
                //TODO zeile weiter oben als aktuelle nehmen
                //Text der aktuellen zeile speichern
                //Text counter erhöhen %
                //alte zeile in R-Farbe schreiben
                //neue zeile in Edit-Farbe schreiben
                //Text in editierzeile übernehmen
                //blink setzen

            }
            else if(keycode == Key.F2) {
                //TODO clear all

            }
        }
        return true;
    }

    @Override
    public void run() {

    }
}
