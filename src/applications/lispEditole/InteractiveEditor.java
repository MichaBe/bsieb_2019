package applications.lispEditole;

import keyboard.Key;
import screen.Screenwriter;

public abstract class InteractiveEditor {

    protected int defaultColor = 0x0f;
    protected int inputlineColor = 0x0b;
    protected int Rcolor = 0x02;
    protected int R2color = 0x09;
    protected int Pcolor = 0x0e;
    protected int errorColor = 0x04;
    protected int usableOutputLineCount;

    protected String[] outputlines;
    protected int[] outputColors;

    protected int inputLineNum;
    protected String curInputLine = "                                                                             ";//77 Leerzeichen
    protected int curInputlineLength = 0;
    protected String helpLine;

    protected boolean needsRedraw;
    protected boolean needsRerun;

    public InteractiveEditor(int usableOutputLines, String helpString) {
        this.usableOutputLineCount = usableOutputLines;
        inputLineNum = usableOutputLines;
        outputlines = new String[usableOutputLines];
        outputColors = new int[usableOutputLines];
        helpLine = helpString;
        for(int i = 0; i < usableOutputLines; ++i) {
            outputlines[i] = new String();
            outputlines[i].copy("                                                                             ");
            outputColors[i] = defaultColor;
        }
    }

    public boolean needsRedraw() {
        return needsRedraw;
    }
    public boolean needsRerun() {
        return needsRerun;
    }

    public void renderOutput() {
        for(int i = 0; i < usableOutputLineCount; ++i) {
            Screenwriter.directPrintString(outputlines[i],0,i,outputColors[i]);
        }
    }
    public void renderInputLine() {
        Screenwriter.directPrintString("> ",0, inputLineNum,inputlineColor);
        Screenwriter.directPrintString(curInputLine,2,inputLineNum,inputlineColor);
    }

    public void renderHelpLine() {
        Screenwriter.directPrintString(helpLine,0,inputLineNum+1,defaultColor);
        needsRedraw = false;
    }

    protected int curCursorPos = 0;

    public boolean handleInput(char character, int keycode) {
        boolean didHandle = true;

        if(keycode == Key.POS1) {
            curCursorPos = 0;
            Screenwriter.setBlinkPosition(curCursorPos+2,inputLineNum);
        }
        else if(keycode == Key.END) {
            curCursorPos = curInputlineLength;
            if(curCursorPos == curInputLine.length())
                curCursorPos--;
            Screenwriter.setBlinkPosition(curCursorPos+2,inputLineNum);
        }
        else if(keycode == Key.ARROW_LEFT) {
            if(curCursorPos > 0) {
                curCursorPos--;
                Screenwriter.setBlinkPosition(curCursorPos+2, inputLineNum);
            }
        }
        else if(keycode == Key.ARROW_RIGHT) {
            if(curCursorPos < curInputlineLength && curCursorPos < curInputLine.length()-1) {
                curCursorPos++;
                Screenwriter.setBlinkPosition(curCursorPos+2,inputLineNum);
            }
        }
        else if(keycode == Key.ENTF) {
            curInputLine.removeAtInplace(curCursorPos);
            curInputlineLength--;
        }
        else if(keycode == Key.BACKSPACE) {
            if(curCursorPos > 0) {
                curCursorPos--;
                curInputlineLength--;
                Screenwriter.setBlinkPosition(curCursorPos+2, inputLineNum);
            }
            curInputLine.removeAtInplace(curCursorPos);
        }
        else if(character != 0) {
            if(curCursorPos < curInputLine.length()-1) {
                curInputLine.insertInplace(character, curCursorPos);
                curInputlineLength++;
            }
            if(curCursorPos < curInputLine.length()-2)
                curCursorPos++;
            Screenwriter.setBlinkPosition(curCursorPos+2,inputLineNum);
        }
        else {
            didHandle = false;
        }

        return didHandle;
    }


    public void run() {
        needsRerun = false;
    }

}
