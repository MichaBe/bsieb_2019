package applications;

import compat.*;
import kernel.Mainloop;

public class KillTask {

    public static int esp;
    public static int ebp;
    public static int esi;
    public static int edi;

    @SJC.Inline
    public static void takeSnapshot() {
        MAGIC.inline(0x89,0x2d);
        MAGIC.inlineOffset(4, ebp);

        MAGIC.inline(0x89, 0x25);
        MAGIC.inlineOffset(4, esp);

        MAGIC.inline(0x89, 0x35);
        MAGIC.inlineOffset(4, esi);

        MAGIC.inline(0x89, 0x3d);
        MAGIC.inlineOffset(4, edi);
    }

    @SJC.Inline
    public static void resetToSnapshot() {
        MAGIC.inline(0x8b, 0x2d);
        MAGIC.inlineOffset(4,ebp);

        MAGIC.inline(0x8b, 0x25);
        MAGIC.inlineOffset(4,esp);

        MAGIC.inline(0x8b, 0x35);
        MAGIC.inlineOffset(4,esi);

        MAGIC.inline(0x8b, 0x3d);
        MAGIC.inlineOffset(4,edi);
    }

}
