package applications;

import compat.*;
import datastructures.SingleIntStruct;
import datastructures.SingleInteger;
import kernel.VirtualMemory;
import keyboard.Key;
import screen.Screenwriter;

public class ProvokePageFault extends Kernelapplication {

    private boolean isRunning;
    private boolean needsRedraw;

    private Screenwriter sw;

    public ProvokePageFault() {
        super("Provoke Page Fault");
        isRunning = false;
        sw = new Screenwriter();
        sw.setColor(0xf,0x0);
    }

    @Override
    public void init() {
        isRunning = true;
        needsRedraw = true;
    }

    @Override
    public boolean run() {
        return isRunning;
    }

    @Override
    public void render() {
        if(needsRedraw) {
            sw.clear();
            sw.println("What kind of Page Fault do you want do produce?");
            sw.println("[0] Page not Present");
            sw.println("[1] Page not Writeable");
            needsRedraw = false;
        }
    }

    @Override
    public Kernelapplication next() {
        return null;
    }

    @Override
    public void handleKeyboardInput(char character, int keycode) {
        if(keycode == Key.ESC)
            isRunning = false;
        else if(character == '0') {
            SingleIntStruct i = (SingleIntStruct)MAGIC.cast2Struct(VirtualMemory.adressNotPressentPage);
            i.n = 42;
        }
        else if(character == '1') {
            SingleInteger i = null;
            i.n = 42;//Lößt Page-fault durch null-Pointer / Pointer auf nichtbeschreibbare Seite aus
        }
    }
}
