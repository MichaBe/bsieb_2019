package screen;

import compat.*;

public class Screenwriter {
    private final static String baseDigits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private final static int width = 80;
    private final static int height = 24;
    private byte currentColor;
    private int vidPosX, vidPosY;
    private static VidChar[] vidmemp1 = new VidChar[80];

    public Screenwriter() {
        currentColor = 0x07;
        vidPosX = vidPosY = 0;
    }

    public static void setBlinkPosition(int x, int y) {
        int pos = y*width+x;
        MAGIC.wIOs8(0x3d4,(byte)14);
        MAGIC.wIOs8(0x3d5,(byte)(pos>>8));
        MAGIC.wIOs8(0x3d4,(byte)15);
        MAGIC.wIOs8(0x3d5,(byte)pos);
    }

    public void setColor(int fg, int bg) {
        currentColor = (byte)((((byte)bg)<<4)|fg);
    }
    public void setColor(int colorCode) {
        currentColor = (byte)colorCode;
    }

    public static void directPrintCharArray(byte[][] charArray, int x, int y, int color) {
        for(int i = x; i < charArray.length && i < 24; ++i){
            for(int j = y; j <charArray[i].length && j < 80; ++j) {
                Screen.textbuffer.lines[i].digits[j].ascii = charArray[i][j];
                Screen.textbuffer.lines[i].digits[j].color = (byte)color;
            }
        }
    }

    public static void clear(byte color) {
        for(int i = 0; i < height; ++i) {
            for(int j = 0; j < width; ++j) {
                Screen.textbuffer.lines[i].digits[j].color=color;
                Screen.textbuffer.lines[i].digits[j].ascii=' ';
            }
        }
    }

    public void clear() {
        for(int i = 0; i < height; ++i) {
            for(int j = 0; j < width;++j) {
                Screen.textbuffer.lines[i].digits[j].ascii = (byte)' ';
                Screen.textbuffer.lines[i].digits[j].color = currentColor;
            }
        }
        setCursor(0,0);
    }

    public void setCursor(int newX, int newY) {
        vidPosX = newX%width;
        vidPosY = newY%height;
    }

    public void backspace() {
        vidPosX -= 1;
        if(vidPosX == -1) {
            vidPosX = width-1;
            vidPosY -= 1;
            if(vidPosY == -1)
                vidPosY = 0;
        }
        directPrintChar(' ',vidPosX,vidPosY,currentColor);
    }

    public void print(char c) {
        Screen.textbuffer.lines[vidPosY].digits[vidPosX].color = currentColor;
        Screen.textbuffer.lines[vidPosY].digits[vidPosX].ascii = (byte)c;
        next();
        setCursor(vidPosX,vidPosY);
    }

    public void print(int x) {
        if(x < 0) {
            print('-');
            x *= -1;
        }
        int curp2PosX = 0;
         do{
            vidmemp1[curp2PosX].ascii = (byte)(x%10+48);
            x /=10;
            ++curp2PosX;
        }while(x != 0);
        for(int i = 0; i < curp2PosX; ++i) {
            Screen.textbuffer.lines[vidPosY].digits[vidPosX].color = currentColor;
            Screen.textbuffer.lines[vidPosY].digits[vidPosX].ascii = vidmemp1[curp2PosX-i-1].ascii;
            next();
        }
        setCursor(vidPosX,vidPosY);
    }

    public void printHex(byte b) {
        int numDigits = 2;
        print("0x");
        for(int i = 0; i < numDigits; ++i) {
            vidmemp1[i].ascii = (byte) baseDigits.charAt(b&0x0F);
            b /=16;
        }
        for(int i = numDigits-1; i >= 0; --i) {
            print((char)vidmemp1[i].ascii);
        }
    }

    public void printHex(short s) {
        int numDigits = 4;
        print("0x");
        for(int i = 0; i < numDigits; ++i) {
            vidmemp1[i].ascii = (byte) baseDigits.charAt(s&0x0F);
            s /=16;
        }
        for(int i = numDigits-1; i >= 0; --i) {
            print((char)vidmemp1[i].ascii);
        }
    }

    public void printHex(int x) {
        int numDigits = 8;
        print("0x");
        for(int i = 0; i < numDigits; ++i) {
            vidmemp1[i].ascii = (byte) baseDigits.charAt(x&0x0F);
            x /= 16;
        }
        for(int i = numDigits-1; i >= 0; --i) {
            print((char)vidmemp1[i].ascii);
        }
    }

    public void printHex(long x) {
        int numDigits = 16;
        print("0x");
        for(int i = 0; i < numDigits; ++i) {
            vidmemp1[i].ascii = (byte) baseDigits.charAt((byte)x&0x0F);
            x /=16;
        }
        for(int i = numDigits-1; i >= 0; --i) {
            print((char)vidmemp1[i].ascii);
        }
    }
    public void print(long x) {
        if(x < 0) {
            print('-');
            x *= -1;
        }
        int curp2PosX = 0;
         do{
            vidmemp1[curp2PosX].ascii = (byte)(x%10+48);
            x /=10;
            ++curp2PosX;
        }while(x != 0);
        for(int i = 0; i < curp2PosX; ++i) {
            Screen.textbuffer.lines[vidPosY].digits[vidPosX].color = currentColor;
            Screen.textbuffer.lines[vidPosY].digits[vidPosX].ascii = vidmemp1[curp2PosX-i-1].ascii;
            next();
        }
        setCursor(vidPosX,vidPosY);
    }

    public void print(String str) {
        for(int i = 0; i < str.length(); ++i)
            print(str.charAt(i));
    }

    public void println() {
        for(int i = vidPosX; i < width; ++i) {
            print(' ');
        }
    }

    public void println(char c) { print(c); println(); }
    public void println(int i) { print(i); println(); }
    public void println(long l) { print(l); println(); }
    public void println(String str) { print(str); println(); }

    public static void directPrintInt(int value, int base, int x, int y, int col) {
        int curX = x, curY = y;
        int curChar = 0;
        if(value < 0 && base == 10) {
            directPrintChar('-',curX,curY,col);
            curX = (curX+1)%width;
            if (curX == 0)
                curY = (curY+1)%height;
            value *= -1;
        }

         do{
            vidmemp1[curChar].ascii = (byte)baseDigits.charAt((value%base) > 0 ?(value%base) : -(value%base));
            curChar++;
            value /= base;
        }while(value != 0);
        for(int i = 0; i < curChar; ++i) {
            directPrintChar((char)vidmemp1[curChar-i-1].ascii,curX,curY,col);
            curX = (curX+1)%width;
            if (curX == 0)
                curY = (curY+1)%height;
        }
    }

    public static void directPrintChar(char c, int x, int y, int col) {
        Screen.textbuffer.lines[y].digits[x].ascii = (byte)c;
        Screen.textbuffer.lines[y].digits[x].color = (byte)col;
    }

    public static void directPrintString(String s, int x, int y, int col) {
        int curY = y;
        int curX = x;
        for(int i = 0; i < s.length(); ++i) {
            directPrintChar(s.charAt(i),curX,curY,col);
            curX = (curX+1)%width;
            if (curX == 0)
                curY = (curY+1)%height;
        }
    }

    private void next() {
        vidPosX = (vidPosX+1)%width;
        if(vidPosX == 0)
            vidPosY = (vidPosY+1)%height;
    }
}
