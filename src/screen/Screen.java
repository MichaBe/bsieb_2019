package screen;

import compat.*;

public class Screen {
    private static boolean curIsTextmode = true;

    public static void switchToVGATextmode() {
        if(!curIsTextmode) {
            BIOS.regs.EAX = 0x0003;
            BIOS.rint(0x10);
            curIsTextmode = !curIsTextmode;
        }
    }
    public static void switchToVGAGraphicmode() {
        if(curIsTextmode) {
            BIOS.regs.EAX = 0x0013;
            BIOS.rint(0x10);
            curIsTextmode = !curIsTextmode;
        }
    }

    public static final Textbuffer textbuffer = (Textbuffer)MAGIC.cast2Struct(0xB8000);
    public static final Videobuffer videobuffer = (Videobuffer)MAGIC.cast2Struct(0xA0000);
}
