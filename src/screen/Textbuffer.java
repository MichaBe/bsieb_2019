package screen;

import compat.*;

public class Textbuffer extends STRUCT {
    @SJC(count=25)
    public TextLine[] lines;
}