package screen;

import compat.*;

public class Videobuffer extends STRUCT {
    @SJC(count = 200)
    public VideoLine[] lines;
}
