package screen;

import compat.*;

public class VideoLine extends STRUCT {
    @SJC(count = 320)
    public byte[] pixel;
}
