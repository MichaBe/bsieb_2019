package kernel;

import applications.*;
import compat.*;
import interrupts.*;
import keyboard.KeyboardLowLevel;
import rte.DynamicRuntime;
import screen.*;

//qemu-system-i386 -boot a -fda BOOT_FLP.IMG
public class Kernel {

    public static void main() {
        DynamicRuntime.init();

        Screenwriter sw = new Screenwriter();
        sw.setColor(0,0xff);
        KeyboardLowLevel.sw = sw;
        Interrupthandler.initPic(sw);
        Interrupthandler.initInterrupts();

        //WICHTIG! nach DynamicRuntime.init() und Interrupt-Initialisierung
        MAGIC.doStaticInit();
        VirtualMemory.init();

/*
        Screen.switchToVGAGraphicmode();
        Screendrawer.Testscreen();

        Timer.waitIntCycles(5);*/
        Screen.switchToVGATextmode();
        sw.setColor(0x9,0xf);
        sw.clear();

        keyboard.State.init();

        KillTask.takeSnapshot();
        Mainloop.loop();
    }
}