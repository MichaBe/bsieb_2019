package kernel;

import applications.Kernelapplication;
import applications.TaskManager;
import keyboard.Scheduler;

public class Mainloop {

    private static Kernelapplication defaultApp;
    private static Kernelapplication curApp;

    static {
        defaultApp = new TaskManager();
        curApp = defaultApp;
        curApp.init();
        Scheduler.currentKeyboardFocus = curApp;
    }

    public static void killCurrentApp() {
        ((TaskManager)defaultApp).remove(curApp);
    }

    public static void loop() {

        curApp = defaultApp;
        while(true) {
            Scheduler.pullInput();
            boolean running = curApp.run();
            if(!running) {
                curApp = curApp.next();
                if(curApp==null)
                    curApp=defaultApp;
                curApp.init();
                Scheduler.currentKeyboardFocus = curApp;
            }
            else {
                curApp.render();
            }
        }
    }

}
