package kernel;

public class Timer {

    public static int interruptCycles;

    public static void waitIntCycles(int intCycles) {
        interruptCycles = 0;
        while(interruptCycles < intCycles);
    }
}
