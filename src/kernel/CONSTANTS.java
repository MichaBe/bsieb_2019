package kernel;

import compat.*;
import datastructures.ImageHeader;

public class CONSTANTS {
    public static final int stackStart = 0x9BFFC;
    public static final int biosbufferAdress = 0x07E00;
    public static final ImageHeader imgHeader = (ImageHeader) MAGIC.cast2Struct(MAGIC.imageBase);

}
