package kernel;

import compat.*;
import datastructures.LinkedObjectList;
import rte.DynamicRuntime;
import screen.Screenwriter;

public class GarbageCollector {
    public static final int mark_INIT = 1;
    private static final int mark_USED = 2;
    private static final int mark_UNUSED = 3;

    public int _totalObjectCount;

    public LinkedObjectList unusedObjects;

    public GarbageCollector() {
        unusedObjects = new LinkedObjectList();
        _totalObjectCount = 0;
    }

    public void mark() {
        _totalObjectCount = 0;
        unusedObjects.clear();
        //Get first object
        Object current = DynamicRuntime.firstObject;

        //mark all as unused
        while (current != null) {
            MAGIC.assign(current._r_gcMark, mark_UNUSED);
            current = current._r_next;
            ++_totalObjectCount;
        }

        //get first heap-object
        Object heapObject = MAGIC.cast2Obj(CONSTANTS.imgHeader.p_heapStart);

        //mark used object
        rec = 0;
        int iNum = 0;
        while(heapObject != null) {//TODO liefert die überprüfung wirklich null zurück, wenn beim letzten heap-object angekommen?
            Recursive(heapObject);
            Screenwriter.directPrintInt(iNum,10,10,19,0xa0);
            iNum++;
            heapObject = heapObject._r_next;
            while(iNum == 27);//27 geht noch, 35 nicht mehr. Sieht nach einer Schleife aus, die zu weit läuft.
        }
    }

    private int rec = 0;

    private void Recursive(Object curObject) {
        //Mark current as used
        if(curObject._r_gcMark != mark_USED) {
            rec++;
            Screenwriter.directPrintInt(rec,10,10,20,0x0a);
            MAGIC.assign(curObject._r_gcMark, mark_USED);
            //get all references, this object holds
            for(int i = 3; i < curObject._r_relocEntries; ++i) {
                int entryAdress = MAGIC.rMem32(MAGIC.cast2Ref(curObject)-MAGIC.ptrSize*i);

                Object entry = MAGIC.cast2Obj(entryAdress);
                if(entry != null) {
                    if(entry._r_gcMark != mark_USED) {//verhindert, dass ein Objekt öfters durchlaufen wird
                        Recursive(entry);
                    }
                }
            }
        }
    }

    public void Collect() {
        Object curObject = DynamicRuntime.firstObject;
        while (curObject != null) {
            if(curObject._r_gcMark == mark_UNUSED)
                unusedObjects.append(curObject);
            curObject = curObject._r_next;
        }
    }
}
