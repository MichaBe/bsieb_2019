package kernel;

import compat.*;
import datastructures.PageTable;
import kernel.CONSTANTS;

public class VirtualMemory {

   public static int adressNotPressentPage;

    public static int getPageTableEntry(int pageTableEntryNum) {
        int pageTabAdresse = ((MAGIC.imageBase+CONSTANTS.imgHeader.imgSize+4095)&~4095)+6*4096;
        PageTable pageTable1 = (PageTable)MAGIC.cast2Struct(pageTabAdresse);
        return pageTable1.entries[pageTableEntryNum];
    }

    public static void setCR3(int addr) {
        MAGIC.inline(0x8B, 0x45); MAGIC.inlineOffset(1, addr); //mov eax,[ebp+8]
        MAGIC.inline(0x0F, 0x22, 0xD8); //mov cr3,eax
    }

    public static void enableVirtualMemory() {
        MAGIC.inline(0x0F, 0x20, 0xC0); //mov eax,cr0
        MAGIC.inline(0x0D, 0x00, 0x00, 0x01, 0x80); //or eax,0x80010000
        MAGIC.inline(0x0F, 0x22, 0xC0); //mov cr0,eax
    }

    public static int getCR2() {
        int cr2=0;
        MAGIC.inline(0x0F, 0x20, 0xD0); //mov e/rax,cr2
        MAGIC.inline(0x89, 0x45); MAGIC.inlineOffset(1, cr2); //mov [ebp-4],eax
        return cr2;
    }

    public static void init() {
        int pageDirAdresse = ((MAGIC.imageBase+ CONSTANTS.imgHeader.imgSize+4095)&~4095)+20*4096;
        int pageTabAdresse = ((MAGIC.imageBase+CONSTANTS.imgHeader.imgSize+4095)&~4095)+21*4096;

        PageTable pageDir00 = (PageTable)MAGIC.cast2Struct(pageDirAdresse);
        PageTable pageTable1 = (PageTable)MAGIC.cast2Struct(pageTabAdresse);

        int flagsPageDir = 0x03;
        int flagsPageTab = 0x03;
        int increment = 4096;

        for(int i = 0; i < 1024; ++i) {
            pageDir00.entries[i] = pageTabAdresse+flagsPageDir;
            pageTable1.entries[i] = (increment*i)+flagsPageTab;
        }
        pageTable1.entries[0] &= 0xFFFFFFFD;//Löschen des Writeable-Bits
        pageTable1.entries[1023] &= 0xFFFFFFFE;//Löschen des Present-Bits
        adressNotPressentPage = pageTable1.entries[1023] & 0xFFFFF000;
        setCR3(pageDirAdresse);
        enableVirtualMemory();
    }
}
