package lisp;

import lisp.data.Expression;
import lisp.data.list.List;
import screen.Screenwriter;

public class EvaluatorPrinter {

    public static Expression evaluate(List rootExpression) {
        if(rootExpression.begin().next != null) //Root-liste hat mehr als ein element -> hinweis auf mismatching )
            return null;
        else
            return rootExpression.begin().expression.evaluate();
    }

    public static void print(Expression expression, String s, int i) {
        expression.print(s,i);
    }
}