package lisp.data.list;

import lisp.data.Expression;
import lisp.data.atom.function.Function;
import screen.Screenwriter;

public class List extends Expression {

    private ExpressionlistEntry first;
    private ExpressionlistEntry last;

    private int curLength;

    public List() {
        first = last = null;
        curLength = 0;
    }

    public void clear() {
        first = last = null;
        curLength = 0;
    }

    public void Append(Expression ex) {
        if(curLength == 0) {
            first = new ExpressionlistEntry();
            first.expression = ex;
            last = first;
        }
        else {
            last.next = new ExpressionlistEntry();
            last.next.expression = ex;
            last.next.previous = last;//Rückverkettung
            last = last.next;
        }
        ++curLength;
    }

    public void Insert(Expression ex, int index) {
        if(index == 0) { //Am anfang anhängen
            ExpressionlistEntry expE = new ExpressionlistEntry();
            expE.expression = ex;
            expE.next = first;
            expE.previous = null;
            first = expE;
        }
        else if(index == curLength) { //Am ende anhängen
            last.next = new ExpressionlistEntry();
            last.next.expression = ex;
            last.next.previous = last;
            last = last.next;
            last.next = null;
        }
        else { //irgendwo in der mitte anhängen
            ExpressionlistEntry prev = first;
            for(int i = 0; i < index-1; ++i) {
                prev = prev.next;
            }
            ExpressionlistEntry next = prev.next;

            ExpressionlistEntry current = new ExpressionlistEntry();
            current.expression = ex;

            prev.next = current;
            current.previous = prev;

            current.next = next;
            next.previous = current;
        }
        ++curLength;
    }

    public ExpressionlistEntry begin() {
        return first;
    }
    public ExpressionlistEntry end() {
        return last;
    }

    @Override
    public Expression evaluate() {
        Expression returner = null;
        if(first.expression instanceof Function) {
            returner = ((Function)first.expression).evaluate(first.next);
        }
        return returner;
    }

    @Override
    public int print(String s, int i) {
        s.value[i] = '(';
        i++;
        ExpressionlistEntry current = first;
        while(current != null) {
            i = current.expression.print(s, i);
            s.value[i] = ' ';
            i++;
            current = current.next;
        }
        s.value[i] = ')';
        i++;
        return i;
    }
}
