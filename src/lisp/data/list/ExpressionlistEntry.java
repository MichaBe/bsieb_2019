package lisp.data.list;

import lisp.data.Expression;

public class ExpressionlistEntry {

    public Expression expression;
    public ExpressionlistEntry next = null;
    public ExpressionlistEntry previous = null;
}
