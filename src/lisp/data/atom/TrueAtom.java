package lisp.data.atom;

import lisp.data.Expression;
import screen.Screenwriter;

public class TrueAtom extends Atom {
    @Override
    public Expression evaluate() {
        return this;
    }

    @Override
    public int print(String s, int i) {
        s.value[i] = 'T';
        return i+1;
    }
}
