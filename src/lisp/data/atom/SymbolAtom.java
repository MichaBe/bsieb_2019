package lisp.data.atom;

import lisp.data.Expression;
import screen.Screenwriter;

public class SymbolAtom extends Atom {

    private Expression value;
    private String name;

    public SymbolAtom(String name, Expression value) {
        this.value = value;
        this.name = new String();
        this.name.copy(name);
    }

    public String getName() {
        return name;
    }

    public void setValue(Expression exp) {
        value = exp;
    }

    @Override
    public Expression evaluate() {
        return value.evaluate();
    }

    @Override
    public int print(String s, int i) {
        for(int j = 0; j < name.length(); ++j) {
            s.value[i] = name.value[j];
            i++;
        }
        return i;
    }
}
