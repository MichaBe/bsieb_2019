package lisp.data.atom;

import lisp.data.Expression;

public class UndefinedAtom extends Atom {

    private String appearence;

    public UndefinedAtom(String appearence) {
        this.appearence = appearence;
    }

    public String getAppearence() {
        return appearence;
    }

    @Override
    public Expression evaluate() {
        return null;
    }

    @Override
    public int print(String s, int i) {
        return i;
    }
}
