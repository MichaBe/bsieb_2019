package lisp.data.atom;

import lisp.data.Expression;
import screen.Screenwriter;

public class NilAtom extends Atom {
    @Override
    public Expression evaluate() {
        return this;
    }

    @Override
    public int print(String s, int i) {
        s.value[i] = 'N';
        s.value[i+1] = 'I';
        s.value[i+2] = 'L';
        return i+3;
    }
}
