package lisp.data.atom;

import lisp.data.Expression;
import screen.Screenwriter;

public class NumberAtom extends Atom {

    private int value;

    public NumberAtom(int f) {
        this.value = f;
    }

    public int getValue() {
        return value;
    }

    @Override
    public Expression evaluate() {
        return this;
    }

    @Override
    public int print(String s, int i) {
        int tempV = value;
        if(value < 0) {
            s.value[i] = '-';
            i++;
            tempV *= -1;
        }
        if(value == 0) {
            s.value[i] = '0';
            i++;
        }
        else {
            i = pRecursive(s,i,tempV);
        }
        return i;
    }

    private int pRecursive(String s, int stringIndex, int number) {
        int aktuelleStelle = number%10;
        int newNumber = (number - aktuelleStelle)/10;
        int curSI;
        if(newNumber != 0)
            curSI = pRecursive(s,stringIndex,newNumber);
        else
            curSI = stringIndex;

        s.value[curSI] = (char)('0'+aktuelleStelle);

        return curSI+1;
    }
}
