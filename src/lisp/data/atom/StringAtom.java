package lisp.data.atom;

import lisp.data.Expression;
import screen.Screenwriter;

public class StringAtom extends Expression {

    private String value;

    public StringAtom(String s) {
        value = new String();
        value.copy(s);
    }

    @Override
    public Expression evaluate() {
        return this;
    }

    @Override
    public int print(String s, int i) {
        for(int j = 0; j < value.length(); ++j) {
            s.value[i] = value.value[j];
            i++;
        }
        return i;
    }
}
