package lisp.data.atom.function;

import lisp.data.Expression;
import lisp.data.list.ExpressionlistEntry;
import lisp.data.list.List;
import screen.Screenwriter;

public abstract class Function extends Expression {

    public static List symbols;
    //TODO Keyboard einbinden

    protected String representation;

    public Function(String rep) {
        representation = rep;

    }

    public abstract Expression evaluate(ExpressionlistEntry iterator);

    @Override
    public int print(String s, int i) {
        for(int j = 0; j < representation.length(); ++j) {
            s.value[i] = representation.value[j];
            i++;
        }
        return i;
    }

    public String getRepresentation() {
        return representation;
    }

}
