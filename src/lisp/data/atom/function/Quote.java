package lisp.data.atom.function;

import lisp.data.Expression;
import lisp.data.list.ExpressionlistEntry;

public class Quote extends Function {

    public Quote() {
        super("QUOTE");
    }

    @Override
    public Expression evaluate(ExpressionlistEntry iterator) {
        if(iterator.next == null)
            return iterator.expression;
        else
            return null;
    }

    @Override
    public Expression evaluate() {
        return null;
    }
}
