package lisp.data.atom.function.arithmetic;

import lisp.data.Expression;
import lisp.data.atom.NumberAtom;
import lisp.data.atom.SymbolAtom;
import lisp.data.atom.function.Function;
import lisp.data.list.ExpressionlistEntry;
import lisp.data.list.List;

public class Plus extends Function {

    public Plus() {
        super("+");
    }

    @Override
    public Expression evaluate(ExpressionlistEntry iterator) {
        int value = 0;
        boolean success = true;
        while(iterator != null) {
            Expression curExpr = iterator.expression;

            if(curExpr instanceof List)
                curExpr = curExpr.evaluate();
            if(curExpr instanceof SymbolAtom)
                curExpr = curExpr.evaluate();

            if(curExpr instanceof NumberAtom)
                value += ((NumberAtom)curExpr).getValue();
            else {
                return null;
            }
            iterator = iterator.next;
        }
        Expression returner = null;
        if(success)
            returner = new NumberAtom(value);

        return returner;
    }

    @Override
    public Expression evaluate() {
        return null;
    }


}
