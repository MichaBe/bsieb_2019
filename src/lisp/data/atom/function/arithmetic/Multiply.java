package lisp.data.atom.function.arithmetic;

import lisp.data.Expression;
import lisp.data.atom.NumberAtom;
import lisp.data.atom.SymbolAtom;
import lisp.data.atom.function.Function;
import lisp.data.list.ExpressionlistEntry;
import lisp.data.list.List;

public class Multiply extends Function {

    public Multiply() {
        super("*");
    }

    @Override
    public Expression evaluate(ExpressionlistEntry iterator) {
        int value = 1;
        while (iterator != null) {
            Expression curExpression = iterator.expression;

            if(curExpression instanceof List)
                curExpression = curExpression.evaluate();
            if(curExpression instanceof SymbolAtom)
                curExpression = curExpression.evaluate();

            if(curExpression instanceof NumberAtom)
                value *= ((NumberAtom) curExpression).getValue();
            else
                return null;

            iterator = iterator.next;
        }
        return new NumberAtom(value);

    }

    @Override
    public Expression evaluate() {
        return null;
    }
}
