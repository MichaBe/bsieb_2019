package lisp.data.atom.function;

import lisp.data.Expression;
import lisp.data.atom.SymbolAtom;
import lisp.data.atom.UndefinedAtom;
import lisp.data.list.ExpressionlistEntry;
import screen.Screenwriter;

public class Setq extends Function {

    public Setq() {
        super("setq");
    }

    @Override
    public Expression evaluate(ExpressionlistEntry iterator) {
        //Voraussetzungen: jedes zweite element ist ein SymbolAtom oder UndefinedAtom, gerade anzahl an elementen
        boolean curNeedstobesymbol = true;
        boolean successCheck = true;
        ExpressionlistEntry cur = iterator;
        while (cur != null && successCheck) {
            if(curNeedstobesymbol)
                successCheck &= (cur.expression instanceof SymbolAtom || cur.expression instanceof UndefinedAtom);

            curNeedstobesymbol = !curNeedstobesymbol;
            cur = cur.next;
        }
        successCheck &= curNeedstobesymbol;
        Expression lastEval = null;
        if(successCheck) {
            cur = iterator;
            while (cur != null) {
                if(cur.expression instanceof SymbolAtom) {
                    SymbolAtom symbol = (SymbolAtom)cur.expression;
                    String curName = symbol.getName();
                    cur = cur.next;
                    boolean found = false;
                    ExpressionlistEntry symbolIterator = Function.symbols.begin();
                    while(symbolIterator != null && !found) {
                        if(((SymbolAtom)symbolIterator.expression).getName().equals(curName)) {
                            lastEval = cur.expression.evaluate();
                            ((SymbolAtom)symbolIterator.expression).setValue(lastEval);
                            found = true;
                        }
                    }
                    if(!found) {//Symbol nicht gefunden: fehler
                        return null;
                    }
                    cur = cur.next;
                }
                else if(cur.expression instanceof UndefinedAtom) {
                    String name = ((UndefinedAtom)cur.expression).getAppearence();
                    cur = cur.next;
                    lastEval = cur.expression.evaluate();
                    Function.symbols.Append(new SymbolAtom(name,lastEval));
                    cur = cur.next;
                }
            }
        }
        return lastEval;
    }

    @Override
    public Expression evaluate() {
        return null;
    }
}
