package lisp.data;
import screen.Screenwriter;

public abstract class Expression {

    public abstract Expression evaluate();

    public abstract int print(String s, int i);

}
