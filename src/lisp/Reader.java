package lisp;

import lisp.data.Expression;
import lisp.data.atom.*;
import lisp.data.atom.function.*;
import lisp.data.atom.function.arithmetic.Divide;
import lisp.data.atom.function.arithmetic.Minus;
import lisp.data.atom.function.arithmetic.Multiply;
import lisp.data.atom.function.arithmetic.Plus;
import lisp.data.list.*;
import lisp.helper.ExpressionStack;
import lisp.helper.Number;

public class Reader {

    private static char[] token = {'\'', '(',')'};
    private static String[] replacement = {" \' ", " ( ", " ) "};


    private List symbolList; // Hier nur Symbol-Atoms einfügen!!
    private Function[] predefinedFunctions;
    private List customFunctions;

    List curHighestLevelList;
    List currentList;

    ExpressionStack stack;

    public Reader() {
        lastError = "";
        curHighestLevelList = null;
        currentList = null;
        symbolList = new List();/*
        Expression nil = new NilAtom();
        Expression t = new TrueAtom();
        Expression s1 = new SymbolAtom("NIL", nil);
        Expression s2 = new SymbolAtom("T", t);
        symbolList.Append(s1);
        symbolList.Append(s2);*/
        customFunctions = new List();
        stack = new ExpressionStack(50);

        predefinedFunctions = new Function[6];
        predefinedFunctions[0] = new Quote();
        predefinedFunctions[1] = new Plus();
        predefinedFunctions[2] = new Minus();
        predefinedFunctions[3] = new Multiply();
        predefinedFunctions[4] = new Divide();
        predefinedFunctions[5] = new Setq();
        //TODO hier weitere funktionen hinzufügen

        Function.symbols = symbolList;
        //TODO weitere Enviroment-Variablen setzen
    }

    public void reset() {
        stack.clear();
        symbolList.clear();
        customFunctions.clear();
        //TODO referenzen zurücksetzen
    }

    private String lastError;
    public String getLastRunstate() {
        return lastError;
    }

    public Expression Read(String input) {
        int curIndex = 0;
        curHighestLevelList = new List();
        currentList = curHighestLevelList;

        //simple " - check
        if(input.count('\"')%2 != 0) {
            lastError = "Error: Missmatching Stringdelimiters \"!";
            return null;
        }
        String[] stringless = input.split('\"');
        int firstString = 0;
        if(input.charAt(0) != '\"')
            firstString = 1;

        int quoteDepth = -1;
        boolean firstStatementAfterQuote = false;


        for(int i = 0; i < stringless.length; ++i) {
            if((i+firstString)%2 == 0) {//Strings behandeln
                StringAtom tempSA = new StringAtom(stringless[i]);
                currentList.Append(tempSA);
            }
            else {//Alles andere behandeln
                stringless[i].replace('(', " ( ");
                stringless[i].replace(')', " ) ");
                stringless[i].replace('\'', " \' ");
                String[] expressionStrings = stringless[i].split(' ');
/*
                for(int j = 0; j < expressionStrings.length; ++j)
                    debugger.println(expressionStrings[j]);*/

                for(int j = 0; j < expressionStrings.length; ++j) {
                    if(quoteDepth != -1 && stack.currentHeight() == quoteDepth) { //Quote ")"-Handling
                        if(firstStatementAfterQuote)
                            firstStatementAfterQuote = false;
                        else {
                            quoteDepth = -1;
                            currentList = (List)stack.pop();
                        }
                    }
                    if(expressionStrings[j].equals("(")) {
                        List l = new List();
                        currentList.Append(l);
                        stack.push(currentList);
                        currentList = l;
                    }
                    else if(expressionStrings[j].equals(")")) {
                        if(stack.currentHeight() == 0) {
                            lastError = "Missmatching brackets!";
                            stack.clear();
                            return null;
                        }
                        else {
                            currentList = (List) stack.pop();
                        }
                    }
                    else if(expressionStrings[j].equals("\'")) {
                        if(quoteDepth != -1) {
                            lastError = "Nested QUOTE-Commands not possible!";
                            stack.clear();
                            return null;
                        }
                        else {
                            List l = new List();
                            l.Append(predefinedFunctions[0]);
                            currentList.Append(l);
                            stack.push(currentList);
                            currentList = l;
                            quoteDepth = stack.currentHeight();
                            firstStatementAfterQuote = true;
                        }
                    }
                    else {
                        //Versuch, als Zahl zu parsen
                        int num = Number.TryParse(expressionStrings[j]);
                        if(Number.lastParseSuccess()) {
                            NumberAtom na = new NumberAtom(num);
                            currentList.Append(na);
                        }
                        else {
                            //funktionen durchsuchen
                            boolean foundSymbol = false;
                            for(int k = 0; k < predefinedFunctions.length && !foundSymbol; ++k) {
                                if(expressionStrings[j].equals(predefinedFunctions[k].getRepresentation())) {
                                    currentList.Append(predefinedFunctions[k]);
                                    foundSymbol = true;
                                }
                            }
                            if(!foundSymbol) {
                                ExpressionlistEntry iterator = symbolList.begin();
                                while(iterator != null) { //Symbole durchsuchen
                                    if(((SymbolAtom)iterator.expression).getName().equals(expressionStrings[j])) {
                                        currentList.Append(iterator.expression);
                                        foundSymbol = true;
                                        break;
                                    }
                                    else {
                                        iterator = iterator.next;
                                    }
                                }
                                if(!foundSymbol) { //custom Funktionen durchsuchen
                                    iterator = customFunctions.begin();
                                    while (iterator != null) {
                                        if(((Function)iterator.expression).getRepresentation().equals(expressionStrings[j])) {
                                            currentList.Append(iterator.expression);
                                            foundSymbol = true;
                                            break;
                                        }
                                        else
                                            iterator = iterator.next;
                                    }
                                }

                                if(!foundSymbol) {

                                    UndefinedAtom ua = new UndefinedAtom(expressionStrings[j]);
                                    currentList.Append(ua);
                                }
                            }
                        }
                    }
                }
            }
        }
        if(quoteDepth != -1 && quoteDepth == stack.currentHeight() && !firstStatementAfterQuote) {//Highest-level-Quote behandeln
            currentList = (List)stack.pop();
            quoteDepth = -1;
        }
        if(stack.currentHeight() != 0) {
            lastError = "Missing )";
            stack.clear();
            return null;
        }
        else {
            stack.clear();
            return curHighestLevelList;
        }
    }
}
