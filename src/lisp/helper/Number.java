package lisp.helper;

import screen.Screenwriter;

public class Number {

    public static int TryParse(String s) {
        parseSuccess = true;
        boolean positive = true;
        int returner = 0;
        if(s.charAt(0) == '-') {
            positive = false;
        }
        else if('0' <= s.charAt(0) && s.charAt(0) <= '9') {
            returner = s.charAt(0)-'0';
        }
        else {
            parseSuccess = false;
            return -1;
        }

        if(!positive && s.length() < 2) {//Einzelnes '-' abfangen
            parseSuccess = false;
            returner = 0;
        }

        for(int i = 1; i < s.length() && parseSuccess; ++i) {
            parseSuccess = ('0' <= s.charAt(i) && s.charAt(i) <= '9');
            if(parseSuccess) {
                returner = returner*10+(s.charAt(i)-'0');
            }
        }
        returner = positive ? returner : returner*-1;
        return returner;
    }

    private static boolean parseSuccess = false;

    public static boolean lastParseSuccess() {
        return parseSuccess;
    }
}
