package lisp.helper;

import lisp.data.Expression;

public class ExpressionStack {

    private Expression[] elements;
    private int top;

    public ExpressionStack(int maxSize)
    {
        top = 0;
        elements = new Expression[maxSize];
    }

    public void push(Expression o) {
        elements[top] = o;
        top++;
    }

    public Expression pop() {
        top--;
        return elements[top];
    }

    public int currentHeight() {
        return top;
    }

    public Expression top() {
        return elements[top-1];
    }

    public void clear() {
        top = 0;
    }
}
