package rte;

import compat.*;

public class SPackage {
  public final static String ROOTPATH = ".";
  public final static char LIMITER = '.';
  public final static char LIMITER_TO_REPLACE = '/';
  
  /*public static class NamedObj {
    private String name;
    private Object item;
    private NamedObj nextObj;
  }*/
  
  public static SPackage root;
  public String name;
  public SPackage outer;
  public SPackage subPacks, nextPack;
  public SClassDesc units;
  //public NamedObj objs;

  public static SMthdBlock findMethod(int rip, SPackage pack) {
    SMthdBlock sm;
    SClassDesc su=pack.units;
    while (su!=null) {
      sm=su.mthds;
      while (sm!=null) {
        int addr=(int)MAGIC.cast2Ref(sm);
        if (addr<=rip && addr+sm._r_scalarSize>=rip) return sm;
        sm=sm.nextMthd;
      }
      su=su.nextUnit;
    }
    pack=pack.subPacks;
    while (pack!=null) {
      if ((sm=findMethod(rip, pack))!=null) return sm;
      pack=pack.nextPack;
    }
    return null;
  }

 /*
  public void printItems(Screenwriter v) {
    SPackage sp=subPacks;
    while (sp!=null) {
      v.print("<pack> ");
      v.println(sp.name);
      sp=sp.nextPack;
    }
    SClassDesc su=units;
    while (su!=null) {
      v.print("<class> ");
      v.println(su.name);
      printCallableMethods(v, su.name, su, false);
      su=su.nextUnit;
    }
    NamedObj so=objs;
    while (so!=null) {
      v.print("<");
      v.print(so.item._r_type.name);
      v.print("> ");
      v.println(so.name);
      printCallableMethods(v, so.name, so.item._r_type, true);
      so=so.nextObj;
    }
  }

  public static boolean register(String fqname, Object obj) {
    if (fqname==null || fqname.count<2) return false;
    //prepare fqname
    String curName;
    SPackage targetPack=root, tmpPack;
    int start, end;
    while ((start = fqname.indexOf(SPackage.LIMITER_TO_REPLACE, 0)) != -1) fqname.value[start] = SPackage.LIMITER;
    //search target package
    start=fqname.value[0]==SPackage.LIMITER ? 1 : 0;
    while ((end=fqname.indexOf(SPackage.LIMITER, start))!=-1) { //search packages
      curName=fqname.substring(start, end);
      tmpPack=targetPack.searchPackWithin(curName);
      if (tmpPack!=null) targetPack=tmpPack; //ok, found package
      else { //did not find package, create new one
        SPackage np=new SPackage();
        np.name=curName;
        targetPack.registerPackWithin(curName, np);
        targetPack=np;
      }
      start=end+1;
    }
    return targetPack.registerObjectWithin(fqname.substring(start, fqname.count), obj);
  }

  public static SPackage searchPack(String fqname) {
    //search target package
    int start, end;
    start=fqname.value[0]==SPackage.LIMITER ? 1 : 0;
    if (start==fqname.count) return root;
    SPackage target=root;
    String curName;
    while ((end=fqname.indexOf(SPackage.LIMITER, start))!=-1) { //search pacakges
      curName=fqname.substring(start, end);
      if ((target=target.searchPackWithin(curName))==null) return null; //did not find curName
      start=end+1;
    }
    return target.searchPackWithin(fqname.substring(start, fqname.count));
  }

  public static String execute(String fqn, String inText, int atPosition) {
    if (fqn==null) return "can not execute null";
    //extract object (class or instance) and method names
    int end=fqn.lastIndexOf(SPackage.LIMITER, fqn.count);
    if (end<1) return "no method name in ".concat(fqn);
    String mthdName=fqn.substring(end+1, fqn.count);
    int start=fqn.lastIndexOf(SPackage.LIMITER, end-1);
    if (start<1) return "no object name in ".concat(fqn);
    String objName=fqn.substring(start+1, end);
    //exctract package names and search them
    end=start;
    start=fqn.value[0]==SPackage.LIMITER ? 1 : 0;
    SPackage pack=SPackage.root;
    int tmp;
    while ((tmp=fqn.indexOf(SPackage.LIMITER, start))<=end) {
      String curName=fqn.substring(start, tmp);
      if ((pack=pack.searchPackWithin(curName))==null) 
        return "could not find package ".concat(curName);
      start=tmp+1;
    }
    //search object within package
    SClassDesc clzz;
    Object obj=null;
    if ((clzz=pack.searchUnitWithin(objName))==null) {
      if ((obj=pack.searchNamedObjWithin(objName))==null)
          return "could not find object ".concat(objName);
      clzz=obj._r_type;
    }
    //search method within object
    SMthdBlock mthd;
    if ((mthd=searchMthdWithinUnit(clzz, mthdName, obj!=null))==null)
      return "could not find method ".concat(mthdName);
    //call it
    System.log.print("calling ");
    System.log.println(fqn);
    System.curText=inText;
    System.curPos=atPosition;
    Kernel.execute(clzz, obj, mthd);
    System.curText=null;
    System.curPos=-1;
    return null;
  }

  public static SMthdBlock findMethod(int rip, SPackage pack) {
    SMthdBlock sm;
    SClassDesc su=pack.units;
    while (su!=null) {
      sm=su.mthds;
      while (sm!=null) {
        int addr=(int)MAGIC.cast2Ref(sm);
        if (addr<=rip && addr+sm._r_scalarSize>=rip) return sm;
        sm=sm.nextMthd;
      }
      su=su.nextUnit;
    }
    pack=pack.subPacks;
    while (pack!=null) {
      if ((sm=findMethod(rip, pack))!=null) return sm;
      pack=pack.nextPack;
    }
    return null;
  }

  private SPackage searchPackWithin(String relname) {
    SPackage sp=subPacks;
    while (sp!=null) {
      if (sp.name.equals(relname)) return sp;
      sp=sp.nextPack;
    }
    return null;
  }
  
  private SClassDesc searchUnitWithin(String relname) {
    SClassDesc su=units;
    while (su!=null) {
      if (su.name.equals(relname)) return su;
      su=su.nextUnit;
    }
    return null;
  }
  
  private Object searchNamedObjWithin(String relname) {
    NamedObj sn=objs;
    while (sn!=null) {
      if (sn.name.equals(relname)) return sn.item;
      sn=sn.nextObj;
    }
    return null;
  }
  
  private static SMthdBlock searchMthdWithinUnit(SClassDesc s, String mthdName, boolean dyna) {
    SMthdBlock sm=s.mthds;
    while (sm!=null) {
      if (sm.namePar.equals(mthdName)) //found method
        return ((sm.modifier&SMthdBlock.M_STAT)==0)==dyna ? sm : null; //return it only if dyna-flag matches
      sm=sm.nextMthd;
      if (sm==null && s.parent!=null) sm=(s=s.parent).mthds;
    }
    return null;
  }
  
  private static void printCallableMethods(Screenwriter v, String obj, SClassDesc s, boolean dyna) {
    SMthdBlock m=s.mthds;
    while (m!=null) {
      if (((m.modifier&SMthdBlock.M_STAT)==0)==dyna && m.namePar.value[m.namePar.count-2]=='(') {
        v.print(' ');
        v.print(' ');
        v.print(obj);
        v.print(LIMITER);
        for (int i=0; i+2<m.namePar.length(); i++) v.print(m.namePar.charAt(i)); //print without brackets
        v.println();
      }
      m=m.nextMthd;
      if (m==null && s.parent!=null) m=(s=s.parent).mthds;
    }
  }
  
  private boolean registerPackWithin(String name, SPackage pack) {
    if (searchPackWithin(name)!=null || searchUnitWithin(name)!=null || searchNamedObjWithin(name)!=null) return false; //avoid double naming
    pack.nextPack=subPacks;
    subPacks=pack;
    return true;
  }
  
  private boolean registerObjectWithin(String name, Object obj) {
    if (searchPackWithin(name)!=null || searchUnitWithin(name)!=null) return false; //avoid double naming
    //search already inserted named objects
    NamedObj sn=objs;
    while (sn!=null) {
      if (sn.name.equals(name)) {
        sn.item=obj; //replace old value
        return true;
      }
      sn=sn.nextObj;
    }
    //none existing, add new named object
    sn=new NamedObj();
    sn.name=name;
    sn.item=obj;
    sn.nextObj=objs;
    objs=sn;
    return true;
  }*/
}
