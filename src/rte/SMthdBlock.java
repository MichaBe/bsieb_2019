package rte;

public class SMthdBlock {
  public String namePar;
  public SClassDesc owner;
  public SMthdBlock nextMthd;
  public int modifier;
  //public int[] lineInCodeOffset;
  public final static int M_STAT = 0x00000020; //"static" modifier, taken from compiler
}
