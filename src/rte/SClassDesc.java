package rte;

public class SClassDesc {
  public SClassDesc parent;
  public SClassDesc nextUnit;
  public SIntfMap implementations;
  public SPackage pack;
  public String name;
  public SMthdBlock mthds;
  public int modifier;
}
