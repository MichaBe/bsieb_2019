package rte;

import applications.Speicherlayout;
import compat.*;
import datastructures.LinkedObjectList;
import interrupts.Interrupthandler;
import kernel.CONSTANTS;
import screen.Screenwriter;

public class DynamicRuntime {
    public static LinkedObjectList ramSegments;

    private static int curByte = 0;
    public static Object firstObject = null;
    private static Object previousObject = null;

    private static final short esValue = (short)(CONSTANTS.biosbufferAdress /16);
    private static final short ediValue = (short)(CONSTANTS.biosbufferAdress%esValue);
    private static int nextcurEBX = 0;
    private static long curSize = 0;
    private static long curBase = 0;

    public static void init() {
        curByte = Interrupthandler.getIDTSize()+CONSTANTS.imgHeader.startAdress+CONSTANTS.imgHeader.imgSize;
        ramSegments = new LinkedObjectList();//TODO initialisieren
    }

    private static void getNextFreePart(int objSize) {
        boolean finishedSearching = false;
        do {
            BIOS.regs.EBX = nextcurEBX;
            BIOS.regs.EAX = 0x0000E820;
            BIOS.regs.EDX = 0x534D4150;
            BIOS.regs.ECX = 24;
            BIOS.regs.ES = esValue;
            BIOS.regs.EDI = ediValue;
            BIOS.rint(0x15);

            if(BIOS.regs.EBX != 0 && BIOS.regs.ECX != 0 && (BIOS.regs.FLAGS&BIOS.F_CARRY) == 0) {
                //Anfrage hat problemlos funktioniert
                if(Speicherlayout.bb.type == 0x01 && Speicherlayout.bb.base >= 0x00100000) {
                    //Speicherbereich frei und über 1MB
                    //Testen, ob der bereich auch mit alinierter adresse ausreicht
                    curSize = Speicherlayout.bb.length;
                    curBase = Speicherlayout.bb.base;
                    curByte = (int)((curByte+3)&~3);
                    if(curByte+objSize <curByte+curSize) {
                        //Speicher ausreichend
                        nextcurEBX = BIOS.regs.EBX;
                        finishedSearching = true;
                    }
                }
                else
                    ++nextcurEBX;
            }
            else {
                break;
            }
        }while (!finishedSearching);
        if(!finishedSearching) {
            //Suche wurde ohne erfolg abgebrochen -> fehlerzustand setzen
            curSize = -1;
        }
        if(Interrupthandler.getIDTSize()+CONSTANTS.imgHeader.startAdress+CONSTANTS.imgHeader.imgSize > curByte) {//Workaround: Bios gibt adresse IM image zurück...
            curByte = Interrupthandler.getIDTSize()+CONSTANTS.imgHeader.startAdress+CONSTANTS.imgHeader.imgSize;
            curByte = ((curByte+3)&~3);
        }
    }

    public static Object newInstance(int scalarSize, int relocEntries, SClassDesc type) {
        //align address
        curByte = ((curByte+3)&~3);
        //calculate memory requirements (in bytes)
        int objSize = scalarSize + relocEntries*4;

        if(objSize+curByte > curBase+curSize) {
            getNextFreePart(objSize);
        }
        if(curSize == -1)
            return null;
        //TODO belegung des speichers in Liste Ramsegmentliste widerspiegeln
        //TODO nochmal überprüfen
        //clear allocated memory
        int dirtyBytes = (objSize+3)&~3;
        while(dirtyBytes > 0) {
            dirtyBytes -= 4;
            MAGIC.wMem32(curByte+dirtyBytes,0);
        }

        //calculate object address inside allocated memory
        int objAdress = curByte+relocEntries*4;
        Object newObj = MAGIC.cast2Obj(objAdress);

        //fill kernel fields of object
        MAGIC.assign(newObj._r_scalarSize, scalarSize);
        MAGIC.assign(newObj._r_relocEntries, relocEntries);
        MAGIC.assign(newObj._r_type, type);

        if(firstObject == null) {
            firstObject = newObj;
        }
        else {
            MAGIC.assign(previousObject._r_next, newObj);
        }
        previousObject = newObj;

        curByte += objSize;
        curByte = (curByte+3)&~3;

        //return object instead of null
        return newObj;
    }

    public static SArray newArray(int length, int arrDim, int entrySize, int stdType,
                                  SClassDesc unitType) { //unitType is not for sure of type SClassDesc
        int scS, rlE;
        SArray me;

        if (stdType==0 && unitType._r_type!=MAGIC.clssDesc("SClassDesc"))
            MAGIC.inline(0xCC); //check type of unitType, we don't support interface arrays
        scS=MAGIC.getInstScalarSize("SArray");
        rlE=MAGIC.getInstRelocEntries("SArray");
        if (arrDim>1 || entrySize<0) rlE+=length;
        else scS+=length*entrySize;
        me=(SArray)newInstance(scS, rlE, MAGIC.clssDesc("SArray"));
        MAGIC.assign(me.length, length);
        MAGIC.assign(me._r_dim, arrDim);
        MAGIC.assign(me._r_stdType, stdType);
        MAGIC.assign(me._r_unitType, unitType);
        return me;
    }

    public static void newMultArray(SArray[] parent, int curLevel, int destLevel,
                                    int length, int arrDim, int entrySize, int stdType, SClassDesc clssType) {
        int i;

        if (curLevel+1<destLevel) { //step down one level
            curLevel++;
            for (i=0; i<parent.length; i++) {
                newMultArray((SArray[])((Object)parent[i]), curLevel, destLevel,
                        length, arrDim, entrySize, stdType, clssType);
            }
        }
        else { //create the new entries
            destLevel=arrDim-curLevel;
            for (i=0; i<parent.length; i++) {
                parent[i]=newArray(length, destLevel, entrySize, stdType, clssType);
            }
        }
    }

    public static boolean isInstance(Object o, SClassDesc dest, boolean asCast) {
        SClassDesc check;

        if (o==null) {
            if (asCast) return true; //null matches all
            return false; //null is not an instance
        }
        check=o._r_type;
        while (check!=null) {
            if (check==dest) return true;
            check=check.parent;
        }
        if (asCast) MAGIC.inline(0xCC);
        return false;
    }

    public static SIntfMap isImplementation(Object o, SIntfDesc dest, boolean asCast) {
        SIntfMap check;

        if (o==null) return null;
        check=o._r_type.implementations;
        while (check!=null) {
            if (check.owner==dest) return check;
            check=check.next;
        }
        if (asCast) MAGIC.inline(0xCC);
        return null;
    }

    public static boolean isArray(SArray o, int stdType, SClassDesc clssType, int arrDim, boolean asCast) {
        SClassDesc clss;

        //in fact o is of type "Object", _r_type has to be checked below - but this check is faster than "instanceof" and conversion
        if (o==null) {
            if (asCast) return true; //null matches all
            return false; //null is not an instance
        }
        if (o._r_type!=MAGIC.clssDesc("SArray")) { //will never match independently of arrDim
            if (asCast) MAGIC.inline(0xCC);
            return false;
        }
        if (clssType==MAGIC.clssDesc("SArray")) { //special test for arrays
            if (o._r_unitType==MAGIC.clssDesc("SArray")) arrDim--; //an array of SArrays, make next test to ">=" instead of ">"
            if (o._r_dim>arrDim) return true; //at least one level has to be left to have an object of type SArray
            if (asCast) MAGIC.inline(0xCC);
            return false;
        }
        //no specials, check arrDim and check for standard type
        if (o._r_stdType!=stdType || o._r_dim<arrDim) { //check standard types and array dimension
            if (asCast) MAGIC.inline(0xCC);
            return false;
        }
        if (stdType!=0) {
            if (o._r_dim==arrDim) return true; //array of standard-type matching
            if (asCast) MAGIC.inline(0xCC);
            return false;
        }
        //array of objects, make deep-check for class type (PicOS does not support interface arrays)
        if (o._r_unitType._r_type!=MAGIC.clssDesc("SClassDesc")) MAGIC.inline(0xCC);
        clss=o._r_unitType;
        while (clss!=null) {
            if (clss==clssType) return true;
            clss=clss.parent;
        }
        if (asCast) MAGIC.inline(0xCC);
        return false;
    }

    public static void checkArrayStore(SArray dest, SArray newEntry) {
        if (dest._r_dim>1) isArray(newEntry, dest._r_stdType, dest._r_unitType, dest._r_dim-1, true);
        else if (dest._r_unitType==null) MAGIC.inline(0xCC);
        else isInstance(newEntry, dest._r_unitType, true);
    }
    /*
    public static int[] arrayDeepCopy(int[] original) {
        if(original == null)
            return null;

        int[] newArray = new int[original.length];
        for(int i = 0; i < newArray.length; ++i)
            newArray[i] = original[i];

        return newArray;
    }*/

    //Compileoption -n
    public static void nullException() {
        Screenwriter.directPrintString("NullPointerException!",30,20,0x0e);
        while(true);
    }
}
