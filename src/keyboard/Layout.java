package keyboard;

public class Layout {
    public static char[] upperCharacters;
    public static char[] lowerCharacters;

    static {
        upperCharacters = new char[93];
        lowerCharacters = new char[93];

        addCharsToUpper(2,"!\"\uc2A7$%&/()=");//TODO
        addCharsToLower(2,"1234567890ß");

        addCharsToUpper(16,"QWERTZUIOP\0*");
        addCharsToLower(16,"qwertzuiop\0+");

        addCharsToUpper(30,"ASDFGHJKL\0\0\0#");
        addCharsToLower(30,"asdfghjkl\0\0\0'");

        addCharsToUpper(44,"YXCVBNM;:_");
        addCharsToLower(44,"yxcvbnm\0\0-");

        upperCharacters[86] = '>'; lowerCharacters[86] = '<';
        upperCharacters[57] = lowerCharacters[57] = ' ';

        addCharsToLower(71,"789-456+1230,");
        addCharsToUpper(71,"789-456+1230,");
        addCharsToLower(53,"/\u0000*");
        addCharsToUpper(53,"/\u0000*");
    }


    private static void addCharsToUpper(int n, String s) {
        for(int i = 0; i < s.length(); ++i) {
            upperCharacters[n+i]=s.charAt(i);
        }
    }
    private static void addCharsToLower(int n, String s) {
        for(int i = 0; i < s.length(); ++i) {
            lowerCharacters[n+i]=s.charAt(i);
        }
    }
}
