package keyboard;

import compat.*;
import screen.Screenwriter;

public class State {

    public static boolean capslookOn;
    public static boolean numOn;
    public static boolean scrollOn;
    public static boolean upperOn;

    static {
        capslookOn = false;
        numOn = false;
        scrollOn = false;
        upperOn = false;
    }

    public static void init() {
        Screenwriter.directPrintChar('S',78,24,scrollOn?0x0f:0x08);
        Screenwriter.directPrintChar('C',77,24, capslookOn ?0x0f:0x08);
        Screenwriter.directPrintChar('N',76,24,numOn?0x0f:0x08);
        Screenwriter.directPrintChar('U',75,24,upperOn?0x0f:0x08);
    }

    public static void handleStateTransition(byte b) {
        byte makeB = (byte)(b&~(byte)0x80);
        if((b&(byte)0x80) == 0) {//Nur Make-Codes untersuchen
            if(b == 58)
                capslookOn = !capslookOn;
            else if(b == 69)
                numOn = !numOn;
            else if(b == 70)
                scrollOn = !scrollOn;

            else if(makeB==42 || makeB==54) {
                upperOn = true;
                Screenwriter.directPrintChar('U',75,24,0x0f);
            }

            byte newB = 0;
            if(capslookOn)
                newB |= 4;
            if(numOn)
                newB |= 2;
            if(scrollOn)
                newB |= 1;

            MAGIC.wIOs8(0x60,(byte)0xED);
            MAGIC.wIOs8(0x60,newB);


            Screenwriter.directPrintChar('S',78,24,scrollOn?0x0f:0x08);
            Screenwriter.directPrintChar('C',77,24, capslookOn ?0x0f:0x08);
            Screenwriter.directPrintChar('N',76,24,numOn?0x0f:0x08);
        }
        else if(makeB == 42 || makeB == 54){
            upperOn = false;
            Screenwriter.directPrintChar('U',75,24,0x08);
        }

    }
}
