package keyboard;

import applications.Kernelapplication;
import interrupts.Interrupthandler;
import datastructures.RBCell;
import screen.Screenwriter;

public class Scheduler {

    public static Kernelapplication currentKeyboardFocus = null;

    private static RBCell tempCell;
    private static int keycode, bytecount;
    private static boolean isMake;
    private static char readCharacter;

    public static void pullInput() {
        if(KeyboardLowLevel.ringbuffer.readable() > 0) {
            Interrupthandler.pauseInterrupts();
            while (KeyboardLowLevel.ringbuffer.readable() > 0) {
                tempCell = KeyboardLowLevel.ringbuffer.read();
                keycode = tempCell.keyCode;
                isMake = tempCell.isMake;
                bytecount = tempCell.byteCount;
                if (currentKeyboardFocus != null && isMake && bytecount != 3) {

                    if (State.capslookOn && !State.upperOn || !State.capslookOn && State.upperOn)
                        readCharacter = Layout.upperCharacters[keycode];
                    else
                        readCharacter = Layout.lowerCharacters[keycode];

                    currentKeyboardFocus.handleKeyboardInput(readCharacter, keycode);
                }
            }
            Interrupthandler.continueInterrupts();
        }
    }
}
