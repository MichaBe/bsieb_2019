package keyboard;

public class Key {
    public static final int ESC = 1;
    public static final int ENTER = 28;
    public static final int STRG = 29;
    public static final int F1 = 59;
    public static final int F2 = 60;
    public static final int F3 = 61;
    public static final int F4 = 62;
    public static final int F5 = 63;
    public static final int F6 = 64;
    public static final int F7 = 65;
    public static final int F8 = 66;
    public static final int F9 = 67;
    public static final int F10 = 68;
    public static final int F11 = 87;
    public static final int F12 = 88;

    public static final int BACKSPACE = 14;
    public static final int ALT = 56;
    public static final int ALTGR = 56;
    public static final int OS_L = 91;
    public static final int OS_R = 92;
    public static final int OPT = 93;

    public static final int ARROW_UP = 72;
    public static final int ARROW_LEFT = 75;
    public static final int ARROW_DOWN = 80;
    public static final int ARROW_RIGHT = 77;

    public static final int END = 79;
    public static final int POS1 = 71;
    public static final int ENTF = 83;
    //TODO ...
}
