package keyboard;

import screen.Screenwriter;
import datastructures.Ringbuffer;

public class KeyboardLowLevel {
    public static Screenwriter sw;

    public static Ringbuffer ringbuffer;

    private static int remainingBytes = 0;
    private static int extraBytesPackage = 0;
    private static int bufferKeycode;

    static {
        ringbuffer = new Ringbuffer(300);
    }

    public static void interruptInput(byte b) {
        //Neues Paket anfangen
        if(remainingBytes == 0) {
            if(b == (byte)0xe0) {
                remainingBytes = 1;
                extraBytesPackage = 1;
            }
            else if(b == (byte)0xe1) {
                remainingBytes = 2;
                extraBytesPackage = 2;
            }
            else {//Normales 1-byte-Package
                boolean isMake = ((b&(byte)0x80)==0);
                int keycode = b&~(byte)0x80;
                ringbuffer.write(isMake,keycode,1);
            }
        }
        else{//Paket fortsetzen
            if(extraBytesPackage == 1) {//Package mit einem extra byte (abschließen)
                boolean isMake = ((b&(byte)0x80)==0);
                int keycode = b&~(byte)0x80;
                ringbuffer.write(isMake,keycode,2);
            }
            else {//Package mit 2 extra bytes
                if(remainingBytes == 1) { //abschließen
                    boolean isMake = ((b&(byte)0x80)==0);
                    int keycode = b&~(byte)0x80;
                    ringbuffer.write(isMake,bufferKeycode*100+keycode,3);
                }
                if(remainingBytes == 2) {   //buffern
                    int keycode = b&~(byte)0x80;
                    bufferKeycode = keycode;
                }
                --remainingBytes;
            }
        }
    }
}
