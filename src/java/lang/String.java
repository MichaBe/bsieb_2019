package java.lang;

import compat.*;
import screen.Screenwriter;

public class String {
    public char[] value;
    public int count;
    @SJC.Inline
    public int length() {
        return count;
    }
    @SJC.Inline
    public char charAt(int i) {
        return value[i];
    }

    @SJC.Inline
    public int count(char symbol) {
        int count = 0;
        for(int i = 0; i < value.length; ++i) {
            if (value[i] == symbol)
                count++;
        }
        return count;
    }

    @SJC.Inline
    public boolean equals(String s) {
        if(s == null || count != s.length())
            return false;

        boolean returner = true;
        for(int i = 0; i < count && returner; ++i) {
            if(value[i] != s.value[i])
                returner = false;
        }
        return returner;
    }
    @SJC.Inline
    public boolean hasSubstringAtIndex(String subString, int index) {
        if(subString == null || count < subString.length()+index)
            return false;

        boolean returner = true;
        for(int i = 0; i < subString.length() && returner; ++i) {
            if(value[i+index] != subString.value[i])
                returner = false;
        }

        return returner;
    }

    @SJC.Inline
    public boolean hasSubstringAsSubstring(int startInThis, String compareTo, int startInCompare, int length) {
        boolean returner = true;
        for(int i = 0; i < length && returner; ++i) {
            if(value[startInThis+i] != compareTo.value[startInCompare+i])
                returner = false;
        }
        return returner;
    }

    @SJC.Inline
    public void replace(char oldChar, String newString) {
        if(newString.length() == 1) {
            for(int i = 0; i < value.length; ++i) {
                if(value[i] == oldChar)
                    value[i] = newString.value[0];
            }
        }
        else {
            int curCount = 0;
            for(int i = 0; i < value.length; ++i) {
                if (value[i] == oldChar)
                    curCount++;
            }
            int neededLength = curCount*(newString.length()-1)+value.length;
            char[] newValue = new char[neededLength];
            int curOldI = 0, curNewI = 0;
            for(int i = 0; i < value.length; ++i) {
                if(value[curOldI] == oldChar) {
                    for(int j = 0; j < newString.length(); ++j) {
                        newValue[curNewI] = newString.value[j];
                        curNewI++;
                    }
                    curOldI++;
                }
                else {
                    newValue[curNewI] = value[curOldI];
                    curNewI++;
                    curOldI++;
                }
            }
            value = newValue;
            this.count = value.length;
        }
    }

    public String[] split(char delimiter) {//TODO debugger entfernen
        int stringCount = 0;
        boolean isAtString = false;
        for(int i = 0; i < value.length; ++i) {
            if(!isAtString && value[i] != delimiter) {//Jeden Anfang eines neuen strings zählen
                isAtString = true;
                stringCount++;
            }
            else if(isAtString && value[i] == delimiter) {
                isAtString = false;
            }
        }
        String[] returner = new String[stringCount];
        int curIndex = 0;
        for(int i = 0; i < stringCount; ++i) {
            while(value[curIndex] == delimiter)
                curIndex++;
            int firstCharacter = curIndex;

            while(curIndex < value.length && value[curIndex] != delimiter)
                curIndex++;
            int lastCharacter = curIndex-1;

            int laenge = lastCharacter-firstCharacter+1;
            returner[i] = new String();
            returner[i].value = new char[laenge];
            returner[i].count = laenge;
            for(int j = 0; j < laenge; ++j) {
                returner[i].value[j] = value[firstCharacter+j];
            }
            curIndex++;
        }
        return returner;
    }

    public void insertInplace(char character, int index) {
        if(index < count) {
            for(int i = count-1; i > index; --i) {
                value[i] = value[i-1];
            }
            value[index] = character;
        }
    }
    public void removeAtInplace(int index) {
        if(index < count) {
            for(int i = index; i < count-1; ++i) {
                value[i] = value[i+1];
            }
            value[count-1] = ' ';
        }
    }

    @SJC.Inline //Deepcopy
    public void copy(String source) {
        value = new char[source.length()];
        count = source.length();
        for(int i = 0; i < count; ++i) {
            value[i] = source.charAt(i);
        }
    }

    public void clearInplace() {
        for(int i = 0; i < count; ++i) {
            value[i] = ' ';
        }
    }

    public void copyInplace(String source, int startIndex) {
        for(int i = 0; i < source.length() && startIndex+i < count; ++i) {
            value[startIndex + i] = source.value[i];
        }
    }

    public void concat(String[] strings) {
        int newLength = value.length;
        for(int i = 0; i < strings.length; ++i)
            newLength += strings[i].length();

        char[] newValue = new char[newLength];
        int curPositionNewString = 0;
        for(; curPositionNewString < value.length; ++curPositionNewString)
            newValue[curPositionNewString] = value[curPositionNewString];

        count = newLength;
        for(int i = 0; i < strings.length; ++i) {
            for(int j = 0; j < strings[i].length(); ++j) {
                newValue[curPositionNewString] = strings[i].value[j];
                curPositionNewString++;
            }
        }
        value = newValue;
    }

    @SJC.Inline
    public void concat(String string) {
        int newLength = value.length+string.length();

        char[] newValue = new char[newLength];
        int curPositionNewString = 0;
        for(; curPositionNewString < value.length; ++curPositionNewString)
            newValue[curPositionNewString] = value[curPositionNewString];

        count = newLength;
        for(int j = 0; j < string.length(); ++j) {
            newValue[curPositionNewString] = string.value[j];
            curPositionNewString++;
        }

        value = newValue;
    }
}