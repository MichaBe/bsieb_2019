package datastructures;

import compat.*;

public class ImageHeader extends STRUCT {
    public int startAdress;
    public int imgSize;
    public int p_descriptorKernelKernel;
    public int p_kernelKernelMain;
    public int p_heapStart;
    public int p_ramInit;
    public int codeOffset;
    public int architekturParam;
}
