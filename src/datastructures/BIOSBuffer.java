package datastructures;

import compat.*;

public class BIOSBuffer extends STRUCT {
    public long base;
    public long length;
    public long type;
}
