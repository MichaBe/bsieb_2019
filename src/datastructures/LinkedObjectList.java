package datastructures;

public class LinkedObjectList {
    private LinkedObjectlistEntry first;
    private LinkedObjectlistEntry last;
    private int Length;

    public LinkedObjectlistEntry begin() {
        return first;
    }
    public LinkedObjectlistEntry end() {
        return last;
    }

    public LinkedObjectList() {
        first = null;
        last = null;
        Length = 0;
    }

    public void append(Object object) {
        if(first == null) {
            first = new LinkedObjectlistEntry(object,null);
            last = first;
        }
        else {
            last.next = new LinkedObjectlistEntry(object,last);
            last = last.next;
        }
        ++Length;
    }

    public Object at(int index) {
        LinkedObjectlistEntry e = first;
        for(int i = 0; i < index && e != null; ++i) {//startet schon mit null, daher < index
            e = e.next;
        }
        if( e != null)
            return e.obj;
        else
            return null;
    }

    public void remove(int index) {
        if(index >= 0) {
            LinkedObjectlistEntry cur = first;
            for(int i = 0; i < index && cur != null; ++i) {
                cur = cur.next;
            }
            if(cur != null) {
                cur.previous.next = cur.next;
                cur.next.previous = cur.previous;
            }
        }
        else {
            //TODO negative indizes zulassen (Python-like)
        }
        --Length;
    }

    public int length() {
        return Length;
    }
    public void clear() {
        first = null;
        last = null;
        Length = 0;
    }
}
