package datastructures;

public class Ringbuffer {

    private int readPosition;
    private int writePosition;

    private RBCell[] cells;

    public Ringbuffer(int size) {
        cells = new RBCell[size];
        for(int i = 0; i < size; ++i)
            cells[i] = new RBCell();
        readPosition = writePosition = 0;
    }
    public void write(boolean isMake, int keycode, int byteCount) {
        cells[writePosition].isMake = isMake;
        cells[writePosition].keyCode = keycode;
        cells[writePosition].byteCount = byteCount;
        writePosition = (writePosition+1)%cells.length;
        if(readable() == 0)
            readPosition = (readPosition+1)%cells.length;
    }
    public RBCell read() {
        RBCell returner = cells[readPosition];
        readPosition = (readPosition+1)%cells.length;
        return returner;
    }
    public int readable() {
        if(writePosition>=readPosition)
            return writePosition-readPosition;
        else
            return writePosition+cells.length-readPosition;
    }
}