package datastructures;

public class LinkedObjectlistEntry {
    public Object obj;
    public LinkedObjectlistEntry next;
    public LinkedObjectlistEntry previous;

    public LinkedObjectlistEntry(Object obj, LinkedObjectlistEntry prev) {
        this.obj = obj;
        this.previous = prev;
        next = null;
    }
}
