package datastructures;

import compat.*;

public class PageTable extends STRUCT {
    @SJC(count = 1024)
    public int[] entries;
}
