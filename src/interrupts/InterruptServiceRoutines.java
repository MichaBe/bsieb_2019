package interrupts;

import applications.KillTask;
import compat.*;
import kernel.Mainloop;
import kernel.VirtualMemory;
import kernel.Timer;
import kernel.CONSTANTS;
import keyboard.Key;
import rte.SMthdBlock;
import rte.SPackage;
import screen.Screenwriter;
import keyboard.KeyboardLowLevel;

public class InterruptServiceRoutines {
    private final static int MASTER = 0x20, SLAVE = 0xA0;
    public static Screenwriter screenWriter;

    @SJC.Interrupt
    public static void withoutParam() {
        screenWriter.println("Without parameter");
        while(true);
    }

    @SJC.Interrupt
    public static void withParam(int param) {
        screenWriter.println("With parameter");
        screenWriter.println(param);
        while(true);
    }

    @SJC.Interrupt
    public static void hwMaster() {
        screenWriter.println("HW-Int Master");
        MAGIC.wIOs8(MASTER, (byte)0x20);
    }

    @SJC.Interrupt
    public static void hwSlave() {
        screenWriter.println("HW-Int Slave");
        MAGIC.wIOs8(MASTER, (byte)0x20);
        MAGIC.wIOs8(SLAVE, (byte)0x20);
    }

    private static final String[] stackRegisters = {"altes EBP","EDI","ESI","EBP","ESP","EBX","EDX","ECX","EAX"};

    @SJC.Interrupt
    public static void BreakpointException() {
        screenWriter.setColor(0xf,0x9);
        screenWriter.clear();
        screenWriter.println("Breakpoint-Exception");

        int ebp=0;
        MAGIC.inline(0x89, 0x6D);
        MAGIC.inlineOffset(1, ebp); //mov [ebp+xx],ebp

        int currentAddress = ebp;

        //Ersten Interrupt zurückgehen:
        for(int i = 0; i < 5; ++i) {
            screenWriter.print(stackRegisters[i]);
            screenWriter.print(": ");
            screenWriter.printHex(MAGIC.rMem32(currentAddress+i*4));
            screenWriter.print(", ");
        }
        screenWriter.println("...");

        //Zweiten Interrupt zurückgehen
        currentAddress = MAGIC.rMem32(currentAddress);
        for(int i = 0; i < 5; ++i) {
            screenWriter.print(stackRegisters[i]);
            screenWriter.print(": ");
            screenWriter.printHex(MAGIC.rMem32(currentAddress+i*4));
            screenWriter.print(", ");
        }
        screenWriter.println("...");

        //Ab hier: normale methoden
        currentAddress = MAGIC.rMem32(currentAddress);

        screenWriter.println("Call-ExpressionStack:___________________");
        int i = 0;
        do {
            screenWriter.print("EBP: ");
            screenWriter.printHex(currentAddress);
            screenWriter.print(", EIP: ");
            int eip = MAGIC.rMem32(currentAddress+4);
            screenWriter.printHex(eip);
            screenWriter.print(", Methode: ");

            SMthdBlock mthd = SPackage.findMethod(eip, SPackage.root);
            if(mthd == null)
                screenWriter.print("-");
            else
                screenWriter.print(mthd.namePar);

            screenWriter.println();

            //neues ebp herausfinden
            currentAddress = MAGIC.rMem32(currentAddress);
            i++;
        }while(i < 10 && 0 < currentAddress && currentAddress < CONSTANTS.stackStart);
        while(true);
    }

    private static int esp;
    private static int ebp;
    private static int esi;
    private static int edi;

    @SJC.Interrupt
    public static void hw_Keyboard() {
        byte b = MAGIC.rIOs8(0x60);
        if(b <= 0xE1) {
            byte tempB = (byte)(b&~(byte)0x80);
            if(tempB == (byte)69 || tempB == (byte)58 || tempB == (byte)70)
                keyboard.State.handleStateTransition(b);
            else if(tempB == 42 ||tempB == 54) {
                keyboard.State.handleStateTransition(b);
                keyboard.KeyboardLowLevel.interruptInput(b);
            }
            else  if(tempB == Key.F12 && tempB == b) {       //Auslösen der Breakpoint-Exception bei F12
                MAGIC.inline(0xCC);
            }
            else if(tempB == Key.F11 && tempB == b) {       //Taskbreak
                MAGIC.wIOs8(MASTER, (byte)0x20);
                //KillTask.resetToSnapshot();

                Mainloop.killCurrentApp();

                ebp=KillTask.ebp;
                esp=KillTask.ebp;
                esi=KillTask.esi;
                edi=KillTask.edi;

               /* MAGIC.inline(0x8b, 0x2d);
                MAGIC.inlineOffset(4,ebp);

                MAGIC.inline(0x8b, 0x25);
                MAGIC.inlineOffset(4,esp);

                MAGIC.inline(0x8b, 0x35);
                MAGIC.inlineOffset(4,esi);

                MAGIC.inline(0x8b, 0x3d);
                MAGIC.inlineOffset(4,edi);*/

                Mainloop.loop();
                MAGIC.wMem32(0xB8000, -1);
                while(true);
            }
            else
                KeyboardLowLevel.interruptInput(b);
        }
        MAGIC.wIOs8(MASTER, (byte)0x20);
    }

    private static int iTimer = 0;
    private static char[] timerSymbols = {'-','\\','|','/'};
    @SJC.Interrupt
    public static void hw_extraTimer() {
        Timer.interruptCycles++;
        iTimer = (iTimer+1)%timerSymbols.length;
        Screenwriter.directPrintChar(timerSymbols[iTimer],79,24,0x0f);
        MAGIC.wIOs8(MASTER, (byte)0x20);
    }

    @SJC.Interrupt
    public static void PageFault(int param) {
        Interrupthandler.pauseInterrupts();
        screenWriter.clear();
        screenWriter.println("PAGEFAULT!");
        screenWriter.print("Parameter: ");
        screenWriter.println(param);

        if((param&4) == 0)
            screenWriter.print("Supervisory process ");
        else
            screenWriter.print("User process ");

        if((param&2) == 0)
            screenWriter.print("tried to read ");
        else
            screenWriter.print("tried to write ");

        if((param&1) == 0)
            screenWriter.println("a non-present page entry.");
        else
            screenWriter.println("a page and caused a protection fault.");

        int cr2 = VirtualMemory.getCR2();
        screenWriter.print("CR2: ");
        screenWriter.printHex(cr2);
        screenWriter.print(" ("); screenWriter.print(cr2); screenWriter.println(")");
        screenWriter.print("PageTable-Entry at CR2: ");
        screenWriter.printHex(VirtualMemory.getPageTableEntry(cr2));
        Interrupthandler.continueInterrupts();
        while(true);
    }
}
