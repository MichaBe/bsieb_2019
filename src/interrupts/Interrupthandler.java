package interrupts;

import compat.*;
import kernel.CONSTANTS;
import screen.Screenwriter;

public class Interrupthandler {
    private final static int MASTER = 0x20, SLAVE = 0xA0;
    private static int tableLimit;
    private final static int tableEntryCount = 48;
    private static Screenwriter screenWriter = null;

    public static int getIDTSize() {
        return tableEntryCount*8;
    }

    public static void initPic(Screenwriter sw) {
        screenWriter = sw;
        InterruptServiceRoutines.screenWriter = sw;
        programmChip(MASTER, 0x20, 0x04); //init offset and slave config of master
        programmChip(SLAVE, 0x28, 0x02); //init offset and slave config of slave
    }
    private static void programmChip(int port, int offset, int icw3) {
        MAGIC.wIOs8(port++, (byte)0x11); // ICW1
        MAGIC.wIOs8(port, (byte)offset); // ICW2
        MAGIC.wIOs8(port, (byte)icw3); // ICW3
        MAGIC.wIOs8(port, (byte)0x01); // ICW4
    }

    public static void initInterrupts() {
        IDT myIDT = (IDT)MAGIC.cast2Struct(CONSTANTS.imgHeader.startAdress+CONSTANTS.imgHeader.imgSize);
        for(int i = 0; i < 48; ++i) {
            myIDT.IDTs[i].segmentSelector = (short)0x8;
            myIDT.IDTs[i].reservedZero = 0;
            myIDT.IDTs[i].type_descriptor = (byte)0x8E;
        }

        int cdRef = MAGIC.cast2Ref(MAGIC.clssDesc("InterruptServiceRoutines"));
        int codeOff = MAGIC.getCodeOff();

        int codeWithoutParam = MAGIC.rMem32(MAGIC.mthdOff("InterruptServiceRoutines","withoutParam")+cdRef)+codeOff;
        int codeWithParam = MAGIC.rMem32(MAGIC.mthdOff("InterruptServiceRoutines","withParam")+cdRef)+codeOff;
        int codeHWMaster = MAGIC.rMem32(MAGIC.mthdOff("InterruptServiceRoutines","hwMaster")+cdRef)+codeOff;
        int codeHWSlave = MAGIC.rMem32(MAGIC.mthdOff("InterruptServiceRoutines","hwSlave")+cdRef)+codeOff;

        int codeHWTimer = MAGIC.rMem32(MAGIC.mthdOff("InterruptServiceRoutines", "hw_extraTimer")+cdRef)+codeOff;
        int codeHWKeyboard = MAGIC.rMem32(MAGIC.mthdOff("InterruptServiceRoutines","hw_Keyboard")+cdRef)+codeOff;
        int codeBreakpoint = MAGIC.rMem32(MAGIC.mthdOff("InterruptServiceRoutines", "BreakpointException")+cdRef)+codeOff;
        int codePageFault = MAGIC.rMem32(MAGIC.mthdOff("InterruptServiceRoutines", "PageFault")+cdRef)+codeOff;

        for(int i = 0; i <= 7; ++i) {
            myIDT.IDTs[i].offsetLowerPart = (short)codeWithoutParam;
            myIDT.IDTs[i].offsetUpperPart = (short)(codeWithoutParam>>16);
        }

        myIDT.IDTs[3].offsetLowerPart = (short)codeBreakpoint;
        myIDT.IDTs[3].offsetUpperPart = (short)(codeBreakpoint>>16);

        myIDT.IDTs[8].offsetLowerPart = (short)codeWithParam;
        myIDT.IDTs[8].offsetUpperPart = (short)(codeWithParam>>16);

        for(int i = 9; i <=12; ++i) {
            myIDT.IDTs[i].offsetLowerPart = (short)codeWithoutParam;
            myIDT.IDTs[i].offsetUpperPart = (short)(codeWithoutParam>>16);
        }

        myIDT.IDTs[13].offsetLowerPart = (short)codeWithParam;
        myIDT.IDTs[13].offsetUpperPart = (short)(codeWithParam>>16); //General Protection Error

        myIDT.IDTs[14].offsetLowerPart = (short)codePageFault;
        myIDT.IDTs[14].offsetUpperPart = (short)(codePageFault>>16); //Page Fault

        for(int i = 15; i <= 31; ++i) {
            myIDT.IDTs[i].offsetLowerPart = (short) codeWithoutParam;
            myIDT.IDTs[i].offsetUpperPart = (short) (codeWithoutParam >> 16);
        }

        //Hardware-Interrupts
        myIDT.IDTs[32].offsetLowerPart = (short)codeHWTimer;
        myIDT.IDTs[32].offsetUpperPart = (short)(codeHWTimer>>16);

        myIDT.IDTs[33].offsetLowerPart = (short)codeHWKeyboard;       //Tastatur
        myIDT.IDTs[33].offsetUpperPart = (short)(codeHWKeyboard>>16);

        for(int i = 34; i <= 39; ++i) {
            myIDT.IDTs[i].offsetLowerPart = (short)codeHWMaster;
            myIDT.IDTs[i].offsetUpperPart = (short)(codeHWMaster>>16);
        }
        for(int i = 40; i <= 47; ++i) {
            myIDT.IDTs[i].offsetLowerPart = (short)codeHWSlave;
            myIDT.IDTs[i].offsetUpperPart = (short)(codeHWSlave>>16);
        }

        //baseAdress und tableLimit anhand von IDT ausrechnen!
        tableLimit = 8*tableEntryCount-1;
        lidtPM();
        MAGIC.inline(0xfb); //Interrupts freigeben
    }

    public static void lidtPM() {
        long tmp=(((long)(CONSTANTS.imgHeader.startAdress+CONSTANTS.imgHeader.imgSize))<<16)|(long)tableLimit;
        MAGIC.inline(0x0F, 0x01, 0x5D);
        MAGIC.inlineOffset(1, tmp); // lidtPM [ebp-0x08/tmp]
    }

    public static void lidtRM() {
        long tmp = (((long)0x0)<<16)|(long)0x3ff;
        MAGIC.inline(0x0f,0x01,0x5d);
        MAGIC.inlineOffset(1,tmp);
    }

    private static int pauseLevel = 0;
    public static void pauseInterrupts() {
        pauseLevel++;
        MAGIC.inline(0xfa);
    }
    public static void continueInterrupts() {
        pauseLevel--;
        if(pauseLevel== 0)
            MAGIC.inline(0xfb);
    }

}
