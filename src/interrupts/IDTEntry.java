package interrupts;

import compat.*;

public class IDTEntry extends STRUCT {
    public short offsetLowerPart;
    public short segmentSelector;           //Alles im Codesegment
    public byte reservedZero;               //Auf 0 setzen
    public byte type_descriptor;   // auf 0x0E setzen für nicht verwendete Interrupts.
                                                // Auf 0x8E setzten für verwendete Interrupts
    public short offsetUpperPart;
}
