package interrupts;

import compat.*;

public class IDT extends STRUCT {
    @SJC(count = 48)
    public IDTEntry[] IDTs;
}
